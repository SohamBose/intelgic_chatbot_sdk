
//global variables

var start = false
var apiUrl = ""
var initPayload = ""
var auto_start = false

//init function


function chatbot_init(arg) {
    start = arg.start
    apiUrl = arg.apiUrl
    initPayload = arg.initPayload
    auto_start = arg.auto_start

    if (apiUrl == "") {
        throw ("Please provide a api string during initialization")
    }

    if (initPayload == "") {
        throw ("Please provide a initial message during initialization")
    }

    if (apiUrl == undefined) {
        throw ("Please provide a api string during initialization")
    }

    if (initPayload == undefined) {
        throw ("Please provide a initial message during initialization")
    }


    if (start) {
        $(document).ready(function () {
            //style of chatbot
            css_style_string = `<style>
    .int_bot_container{position: fixed; bottom: 3px; right:20px; font-family: sans-serif;  z-index: 999999;}
    .int_bot_action{ width: 50px; height: 50px; border-radius: 50%;background:#e94343; border: 0; padding: 5px;float:right; margin: 10px 0; cursor: pointer; box-shadow: #333333 0 0 5px;}
    .int_bot_action img{width: 65%;}
    .int_bot_action:focus{outline: none;}
    .int_bot_action:hover{background:#e94343;}
    .clr{clear: both;}
    .int_bot_footer{padding:10px 15px;background:#efefef; border-radius: 0 0 8px 8px; display: flex;  justify-content:space-between; }
     textarea.int_bot_new_message{background:none; border: 0; resize: none;  font-family:  sans-serif;  font-size: 16px; width: 100%; height: 22px; min-height: 22px;
        padding: 0 !important; border-radius: 0 !important; margin: 0 !important;}
    textarea.int_bot_new_message:focus{border: 0; box-shadow: none; outline: none;}
    .int_bot_new_message:focus{outline: none;}
     button.int_bot_send{border: 0; display: none; padding: 0 !important; border-radius: 0 !important; background: none !important;
        font-size: inherit !important; }
    .int_bot_header{background: #e94343; padding:10px 15px;border-radius: 8px 8px 0 0; } 
    .int_bot_header h2{padding: 0; margin: 0; font-size: 18px;color: #f5faff;} 
    .int_bot_header span{padding: 0; margin:3px 0 0 0; font-size: 13px;color: #39fd5a;} 
    .int_bot_box{width: 350px;  background: #fffeff; box-shadow: rgba(63, 63, 63, 0.5) 0px 0px 10px; border-radius: 8px; display: none;}
    .int_bot_conversation{padding: 15px; max-height: 45vh; min-height: 45vh; overflow: auto; font-size: 16px; line-height: 20px;}
    .int_msg_out{ margin-bottom: 15px;} 
    .int_msg_out_con{ max-width: 80%; float: right;}
    .int_msg_out_con p{background: #ff7e82; border:#ff7e82 solid 1px; border-radius: 5px 5px 0 5px; color: #f6fbff;padding: 5px 10px 5px 10px; margin: 0;}

    .int_msg_in{ margin-bottom: 15px;}
    .int_msg_in_con{max-width: 80%; float: left;}
    .int_msg_in_con p{background: #f7ebee; border:#efeeef solid 1px; border-radius: 0 5px 5px 5px; color: #3b3a3b;padding: 5px 10px 5px 10px; width: margin: 0;}
    .int_msg_option{background: #ff7e82;  border-radius:15px; color: #f6fbff;padding: 5px 10px 5px 10px; text-decoration: none; display: inline-block; margin: 3px 0; }
    .int_msg_option:hover{background: #e94343;  color: #f6fbff;text-decoration: none;  }
    .int_smg_img{width: 100%;}
    /* .int_typing_icon{ padding: 0 10px !important; background: #fdfdfd !important; border: 0 !important;} */
    .int_back_btn{background: #ff7e82;  border-radius:15px; color: #f6fbff;padding: 5px 10px 5px 10px; text-decoration: none; }
    .int_back_btn:hover{background: #e94343; }
    .int_waiting_icon { margin-bottom: 15px;}
    .int_quick_reply_btn { border-radius: 50%; width: 20px; height: 20px; border: #fff solid 1px; float: left;}

    .int_typing_icon {
        margin: 10px auto 0;
        width: 70px;
        text-align: center;
    }
    
    .int_typing_icon > div {
        width: 18px;
        height: 18px;
        background-color: #333;
    
        border-radius: 100%;
        display: inline-block;
        -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both;
        animation: sk-bouncedelay 1.4s infinite ease-in-out both;
    }
    
    .int_typing_icon .bounce1 {
        -webkit-animation-delay: -0.32s;
        animation-delay: -0.32s;
    }
    
    .int_typing_icon .bounce2 {
        -webkit-animation-delay: -0.16s;
        animation-delay: -0.16s;
    }
    
    @-webkit-keyframes sk-bouncedelay {
        0%, 80%, 100% { -webkit-transform: scale(0) }
        40% { -webkit-transform: scale(1.0) }
    }
    
    @keyframes sk-bouncedelay {
        0%, 80%, 100% { 
        -webkit-transform: scale(0);
        transform: scale(0);
        } 40% { 
        -webkit-transform: scale(1.0);
        transform: scale(1.0);
        }
    }
        </style>`

            //inclding style and css
            $('head').append(css_style_string);
            $('head').append('<link href="https://fonts.googleapis.com/css2?family=Raleway:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">');
            //including chatbot div
            str_chatbot_div = `<div class="int_bot_container">
        <div class="int_bot_box">
            <div class="int_bot_header">
                <h2>Ask Ruby</h2>
                <span>Online</span>

            </div>

            <div class="int_bot_conversation">
                
            </div>

            <div class="int_bot_footer">
                <textarea class="int_bot_new_message" name="message" placeholder="Type a message..."
                    autocomplete="off"></textarea>

                <button type="submit" class="int_bot_send"><img alt="send" width="25px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAKp0lEQVR4Xu1baWxU1xU+961+M14wGErDUmxsDDYYgwlFbUBRImiJ8qdKRRIaN5TICciEUKqmLT9aqaoa1DQJa5pWqE23SF2iSpSgKg2pWqniR2hSUQxeAa+MN4w925t5y+133wyWMa0ZzAy2wdd6njdvmXfPd8/ynXvPI5pu0whMIzCNwP2MwIcPLj53cve2I2c/+mfhfYlD/SKFt5X7w6eLlJ6Tm9adqn/3xKb7CQh2frnBlQgnySaSVYlfdflg76yFrdlbtv/qoZe/9fq9DgZrXqpzbsVJcgACYxTnRDFVIVNVY5YkDXRvqP7T2ud3HJxfubLhXgTjJgAcl8iSJXJklWxJorCj8RyF9QeKSs4b2/ccWf/lrX+4l4BgF0t17thxYi4nGZK5AICRRAThXY69LJuGQkQ+v0acx6gna+5l/SvfPLKgZuehT8/0WVMdDHaxROMuABANFkBcAAAzUPCFu+KATrYR9Y47Fr7rBg2SxXtnzGtmlY/8o7T2xR8Xr1xVP1WBYK3FGrcBAAabOAZeaIAEAKAAOIANzjGKE4acQy5FydZdUnGxHcXg56pwnr7+tvlLLuTW7j2w9omn351qQLC2YoVbtp0EgJHDhdQJAMSuomeTEgqRrAEIB8egLAq0wIJK6I5NQckhIwv+wtaCjdlz243ttT/duHvvoakCBLtcokADMMyeCTBoQAIAoQ0CAFk4RaH+WUQGvIRhahR2LYoqNuWpGpkytAegcEWHNrgky7p5MW9WW9/6jaeqavbsLy1f3jaZwWDNJTJ3HcdTexnu7zoA2PUAkPAZx/DLrnCKMUQHTkYSmEF4TZ9tkONGSVUTYsZwzpV9FCfFthgfCi986Ez+C1t/tnJL9aQ0D3ZxscTRIKzYEo4wnc1WoEGWL9I1u/SCvvVrvy6urj48vyAfOjU5WsYBUKEaAcciH4DIc7IjTcx/tfvRRz6sfH7vD8pWP9g00TDcAEAmOhOFf/DlGGTaMU/LslU/mY7kDGZpfbGC3ED27kP7Vn/p8ZOZeHYqv5lxALLijBRFoRDiaQRxVfgPQZ/gS+laDswjKlNoZsXHyldfePvhF3ccTqXT6byGtRQxzweMbCIajD423of6ZYX6YjYYJZEGp2lH4WwBiM58ZEfiFPKbMA2iiC1Rx4wFl2MbNr9ftK3mQPmaqgvjfebt3JdxACyEUwQOYoi0IsIqhoYIgb8YkV+HNsRl6lAcykekcWIAJEcj3SwImHPKG6SXnjtateWpjOYeNwEgRt/jAaO04nZQHX2tSBhEmJTg+22A4EATJBNO0ZVpUHaISTJp5CcwMrLIJAd24kJLXIdRQF3QlLez5q3ZT1X/pLBgHvQnvY01F3qEd7ilG4A4KKUSdykLQsZd6DoIhogMFvZjiAwaAweBo7QEKDhrQFMiAEts+XGJggbodlR1A9kF7dFHN58seXbHG6WVK9MWPdilIo8FAwUQHcF+xD60QHxzkRhwECAmhg4dxICB9Ah1hmPDUMKsKa7gIHc8DoFLvU00kVAJ1ZeThMrBCYcwqjjAYQ8K7hE/G8PtCq5TueyxTqF5DsNzcZ8jklLBQoVPQnruOCoNSjl95rLKM7Orn/1FxZNP//5O9YGdWV7AuaByDuZCHFeRGfcrntE6AMAmXaTJ+JRBc9GvhJAK4BHXgD5K0O/krsefvaQKQDLsCzhDmoXfgIAYfHFdAmChBx5MeAyYpGxDyDgB1wQZw59MOjYVWhLEUCQYqYggGjQkxhUa1HPNiJHT6dS8fGD1liff8s8uSPD522zDvK/rWsB3tbtn3rXLrWXhxktlocbmskhnz6L59R+s0jiYPuiyjvRYkiQGrVYdJ8ws2yRjWBiAghNi1DgSJQXaIwS2uEYqhhHpktc14Q+EH/CSDWiEBIco3I63icMCgOtA4VOHs3QVRpYKvyAgS17r4H4bNNNkGsLprI7gFzafWF77jVeWFJbcVu6RMvFt6rwyo7f9cnmwpWWZ29hYTg31pdTevmDOwMdlCnooQ7eZUA/YOoeYHAmTBNByRKKEjNEGGjZ0HH7NAwaOnzSgJdJrYVY21ELAJLkK/jtwivgGDYvDXDiw80xMmOF104DJKEwlpjG6FoqSawB8C3fNX3s6d8939i97YvPxVJQhZQDG+rEr3Sbr6e0s6m1rWhm+WF/FG+rWaI1NJWp375wCp9nPEeMZNk+5MYQy7F92Y9jgB8gH4cQnuAKQgeF5WiLcpQBLNCSeSe1Duq4AMKFlOOb5EABjQHgjplHExME8PItJ1JpdXK8/U/vmhpd2jkmu0gLAWOD0XbWkoaGhnJ5A5+L+1qaKWPP5z+oN56r8zfWLje7ATF2NkJ1MwVVZhlvhpNoWwLGEyyRLmA8coAQ0Ypi8Fak5LiMNJiBDHSwD2oWZKszckeQn6sHkbk4UXALne/NM6pYf6NA2PnZ8ybba1xdVrGoZ3deMA3ArNQx3BnL6+vuLOzrblvW3NK2zGuvW+ZrqluR3NOXNCA545gL/m9AEgKHAjGR8wrq8jUdApmbJNBAXPgrkCtcNITyF8ZlvGRR34qRmK04gbJmhpes+Wrjru68se/yL71/v14QDcCuAnNa6wmsdHeWR5sYqt+HcJrvu358zW+qJBQcpC2RJwYJGCKTKwSyVgjClRmIewwwlKAdFNQnaocAPCe0Cv7BYqGte4SVj63PHVz2z641k1L5VNyb8fDDZgzsdMEA1TPxAw0hM805sgwnMhQk8kDSBz8ME1mTABMIwgThM4JOkCXxw10wg6QTz4QQXwAmWJ53gWjjBUjjBGRl0ghxO0IITbIMTPAEn+CqcYFdGnGAyDBYjDJYhDFYiDK5GGFyCMPgphMH8CQiDQwiDLQiDBxEGfzmWjqdsAiBCuSBCFSBCK0CEVoAILQUR+gyIUBGIEEISplQnnggFQYT+BSK0D0TodCrGfQMA5xrOCiq8OkmFV4AKF4IKrwMVlkZQYW/NAFSYJgkVjoEKd4IKvwcqvA9UGAt5qbfryVAI1CuGZCgbyZA+RZKhEJKhgEiG1tfWHk1d5BuvnKrp8CdIh4+lJR2eIhMihAmRK5gQeQ8TIt/HhEj7eEf8pigwelI03TNC4oF3OCXWhSmxo2t3ffuH6RJ65O9M1knRICZFL2BS9DAmRX+TCcGHidAkmxYfwLT4XzAtvh/T4mczKfgwACPXBjPxwBQXRs5jYeTnWBh5LRN9GJMIZRqA/7M0Rlgai2BprAtLY3uxNPbnuy34/9SATKwOj1ocdbA4OojF0VNYHN2HxdHmiRL8rgGQXB7nWB6vx/L42w9/ffePJlroG6JABgskkGzzULJA4hgKJH47mQQf1oAMlMjEUSITQInMX1Ei8z2UyHRORsGHAUhjkZQ5okjqrnvz8YKcjjK5AZTJNSTL5H433o5M1H3jLZQkFEpeQqHk31EouR+FklO2jng8pbJdKJU9iFLZV1Eqe2NlxUQN4x08N5ViaUKxdAjF0nUolj6EYul37uB5k+7WscrlsSojhVEu/0eUy7+Gcvn/TLrep6FDo1+YMPHCRAwvTLThhYl3imt2HZqb78fayz3ckq/McLwyM4hXZv6GV2Yeu4fFvVk0vDTF8dLUMbw0VXFfCT4t7DQC0whMIzCNANF/AToTPnNgIzIwAAAAAElFTkSuQmCC"></button>

            </div>

        </div>

        <button type="button" class="int_bot_action">
            <img id="open_close" value="chatting" alt="chatting" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6OUVFNkVEMUM4MTZCMTFFQkFGODVGNDA4ODkwRDcwNTIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6OUVFNkVEMUQ4MTZCMTFFQkFGODVGNDA4ODkwRDcwNTIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo5RUU2RUQxQTgxNkIxMUVCQUY4NUY0MDg4OTBENzA1MiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo5RUU2RUQxQjgxNkIxMUVCQUY4NUY0MDg4OTBENzA1MiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PnFA92MAAAIPSURBVHja3NdPSBRxFMDxmWVVrF0worYgS0mK8FCBNy+CHvbStbzULeoQaNDZi7dIiC4dFKNDB4+i0KGLLHiQoEWEoENIC1qBSKK1f6r9+f3pG1jG2Z3f7Iw74IMP7ujs+pv3e+/tjK2UsuKMhBVzOAsYwQrKUBH5jKTfAmy2YJifH/RrbMgiwsYltOMMfjU8kwUsq8O4p+shInn5zC6/c/UW3MZPzMVVA50oxlWE9YrkNHLoNfyc37iPpajaUP8+HfBCklFmYBfXIsz0C9yU1xXk8RJbllTreoQd4O6CBArqaPzAlUZpeyL9bBIlvJZuckcV13FRjjvwFA8xVS8DGVRVsHgUYA4ksYOtehnQVzKEjGEG/sg0NY1/2EZPoy3IhSg62xm0cXwb6vl/9aDCLWun2TbUcRmD6K65Ir9ow12k8MboHR5FeAqvUFHNxwJSPq2q/6dyZ0B/Mb3DDUmhvoqvJntZU1xr+Gi8YbLib3iGshwv4nzEw8kzA5YrdUWMwXadPIHR41qAM3BW0e9xYlr+/uk4FqBr4Dn+Y1JGqjuK4oK0bTXKnrUNb8vfI4tpzOKvaYmhIAXtjnU9CU3T1YfvIdpyDXe8tsAO8GByFo/RH+DmIyFT8ZYcz2Bc7qACZSCsLDYlG18wUNsFrXIO87IIPWVLrV6A4wH2nOKwY3o41aP+bZA2PLlPx/sCDADoseJG8BQJkAAAAABJRU5ErkJggg==">
        </button>

        </div>`
            $('body').append(str_chatbot_div);
            //fucntion to always scroll on buttom
            function chatbot_container_auto_scroll() {
                var messageBody = document.querySelector('.int_bot_conversation');
                messageBody.scrollTop = messageBody.scrollHeight - messageBody.clientHeight;
            }

            request_body = {
                "sender": "",
                "message": ""
            }

            if (localStorage.getItem("username")) {
                previous_user = 1
                request_body.sender = localStorage.getItem("username")
            } else {
                previous_user = 0
                username = GetUserName();
                localStorage.setItem("username", username);
                request_body.sender = username
                previous_conversations = []
            }


            $(".int_bot_action").click(function () {
                $(".int_bot_box").toggle();
                $('.int_bot_conversation').empty();
                switch ($('#open_close').attr('value')) {
                    case 'chatting':
                        $('#open_close').attr('src', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RDM0OUVGMjc4MzIyMTFFQkEzQjNCMTMzQUIwQzBCNkEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RDM0OUVGMjg4MzIyMTFFQkEzQjNCMTMzQUIwQzBCNkEiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpEMzQ5RUYyNTgzMjIxMUVCQTNCM0IxMzNBQjBDMEI2QSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpEMzQ5RUYyNjgzMjIxMUVCQTNCM0IxMzNBQjBDMEI2QSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Ppx79EkAAAKeSURBVHja3JvPTsJAEIcrEhVuPAkXQzTeQOQ/8YAHH8IYEy+e1afwxkEOejDqQ3gz8aRH9RVURCV1Rkui0Ha37c50dyf5haS7hX4f0DQ7bcZ13TxkF3IHOYEUIY6lWYEMIM+QQ0gBNx64/2sIqVkI34WMpliPceDBnS3bJPjBYz1mHMe5dWZrCXIBqTnmVxdyBlnwGbtBO6uQT9e/TP8lBH3zrrd9eTKxJ5CwYSE8jjt/d7BJghT8tABbJEjD+wkwXUIk+CABpkqIDB8mQEZC1XR4kQBTJMSGlxGgu4RE8LICdJWQGD6KAJGEN2YJSuCjCpCRsG4SfBwBaUtQCh9XQFoSlMMnEcAtgQQ+qQAZCRWd4VUIoJZACq9KAJUEcniVAlRLYIFXLUCVBDZ4CgEyEsq6wFMJiCuBHZ5SQFQJqcBTCxBJePUkpAaPmfuxQFs9yACS9RkbQuYDmhYfkC3IJeXBZRk6M+feq5+EXMA+LPBYGab2FErYhnxJzGWD5xQgK4EVnlsA1gtkHDI+9uY4Ngqoex3nxZA5eE64gpRtEyADP6k85JpLQiZl+FHAOSHHJYFaQEMAvxlyYpxIqJAeIeFVVgPyHnCFh9vrzCtLrJfCUeBTlaALfGoSuOEbmiy0kghoKoBnX3LXEZ5Vgq7wbBJ0hmeRkOTAWgL4JuPyWpVbACc8qYS48CNmeLI7VUyCJ5GgEr7FeIuMMgmyH9gWLF1zwiu9mdNUeGUSTIZXIsF0+MQSgt6wI4Bva3izdCwJtsDLSqiJBJgMH0uCbfCRJcjCdwyCjyQBJ5YshJeVUMS+wF5If77ntapMrbCGLD4cuoMC1iyFl5FQQgF9i+FFEvr4PylAjiBP7u+j5SWD//Oi4GPCp5B7yD4k/y3AAEFNMoJrnmCNAAAAAElFTkSuQmCC');
                        $('#open_close').attr('value', 'close')
                        $('.int_bot_action').css("background-color", "black");
                        break;
                    case 'close':
                        $('#open_close').attr('src', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6OUVFNkVEMUM4MTZCMTFFQkFGODVGNDA4ODkwRDcwNTIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6OUVFNkVEMUQ4MTZCMTFFQkFGODVGNDA4ODkwRDcwNTIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo5RUU2RUQxQTgxNkIxMUVCQUY4NUY0MDg4OTBENzA1MiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo5RUU2RUQxQjgxNkIxMUVCQUY4NUY0MDg4OTBENzA1MiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PnFA92MAAAIPSURBVHja3NdPSBRxFMDxmWVVrF0worYgS0mK8FCBNy+CHvbStbzULeoQaNDZi7dIiC4dFKNDB4+i0KGLLHiQoEWEoENIC1qBSKK1f6r9+f3pG1jG2Z3f7Iw74IMP7ujs+pv3e+/tjK2UsuKMhBVzOAsYwQrKUBH5jKTfAmy2YJifH/RrbMgiwsYltOMMfjU8kwUsq8O4p+shInn5zC6/c/UW3MZPzMVVA50oxlWE9YrkNHLoNfyc37iPpajaUP8+HfBCklFmYBfXIsz0C9yU1xXk8RJbllTreoQd4O6CBArqaPzAlUZpeyL9bBIlvJZuckcV13FRjjvwFA8xVS8DGVRVsHgUYA4ksYOtehnQVzKEjGEG/sg0NY1/2EZPoy3IhSg62xm0cXwb6vl/9aDCLWun2TbUcRmD6K65Ir9ow12k8MboHR5FeAqvUFHNxwJSPq2q/6dyZ0B/Mb3DDUmhvoqvJntZU1xr+Gi8YbLib3iGshwv4nzEw8kzA5YrdUWMwXadPIHR41qAM3BW0e9xYlr+/uk4FqBr4Dn+Y1JGqjuK4oK0bTXKnrUNb8vfI4tpzOKvaYmhIAXtjnU9CU3T1YfvIdpyDXe8tsAO8GByFo/RH+DmIyFT8ZYcz2Bc7qACZSCsLDYlG18wUNsFrXIO87IIPWVLrV6A4wH2nOKwY3o41aP+bZA2PLlPx/sCDADoseJG8BQJkAAAAABJRU5ErkJggg==');
                        $('#open_close').attr('value', 'chatting')
                        $('.int_bot_action').css("background-color", "#e94343");
                        break;
                }
            });
            //sring temp for further use
            str_url = apiUrl


            str_int_msg_out = '<div class="int_msg_out">'
            str_int_msg_in_con = '<div class="int_msg_in_con">'
            str_clr = '<div class="clr"></div></div>'
            str_clr_optn = '<div class="clr"></div>'
            str_int_msg_in = '<div class="int_msg_in">'
            str_int_msg_out_con = '<div class="int_msg_out_con">'
            str_waiting_icon = `<div class="int_typing_icon">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>`
            str_int_msg_out_icon = '<div class="int_waiting_icon">'

            function bot_text_res(text) {
                text_str_response = `<p class="res">${text}</p></div>`
                $('.int_bot_conversation').last().append(str_int_msg_out);
                $('.int_msg_out').last().append(str_int_msg_in_con);
                $('.int_msg_in_con').last().append(text_str_response);
                $('.int_msg_in_con').last().after(str_clr);
            }


            // creating button function
            function bot_button_res(btns_array) {
                let button = btns_array
                $('.int_bot_conversation').last().append(str_int_msg_out);
                $('.int_msg_out').last().append(str_int_msg_in_con);
                button.forEach(function (item, index) {
                    btn_text = item.title
                    btn_playload = item.payload
                    btn_image = item.image_url
                    if (btn_text == 'Go Back') {
                        bot_back_button_res(btn_text, btn_playload)
                    } else {
                        if(btn_image == undefined){
                            text_btn_response = `<a class="int_msg_option res" href="${btn_playload}">${btn_text}</a>`
                        }else{
                            text_btn_response = `<a class="int_msg_option res" href="${btn_playload}">
                                                 <img src="${btn_image}" class="int_quick_reply_btn" />&emsp;${btn_text}
                                                </a>`
                        }
                        $('.int_msg_in_con').last().append(text_btn_response);
                    }
                    $('.int_msg_in_con').last().after(str_clr);
                });
            }

            //creating back button function
            function bot_back_button_res(text, payload) {
                $('.int_bot_conversation').last().append(str_int_msg_out);
                $('.int_msg_out').last().append(str_int_msg_in_con);
                text_btn_response = `<a class="int_back_btn res" href="${payload}">${text} <img width="13" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RjMxNDEyNDg4ODg2MTFFQjkyMjU4NDM5QkMzNURFMUIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RjMxNDEyNDk4ODg2MTFFQjkyMjU4NDM5QkMzNURFMUIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpGMzE0MTI0Njg4ODYxMUVCOTIyNTg0MzlCQzM1REUxQiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpGMzE0MTI0Nzg4ODYxMUVCOTIyNTg0MzlCQzM1REUxQiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PhUmcSUAAAFgSURBVHjaYvz//z8DLQETA40BNgv4gfgAEM+nhgUsaHxWIF4HxPZA/JUqXgDFARJe9B8C7gKxKJocWRiZ0wQ1/A0Qq1HDcGQLkqCGfwdiK2oZDsKMQCIKGFKLoBHeBsRHSA1lIH4AxLegbIw4+PGfOuATEO8AYh10H+wH2uMAxN+B+BAQ/yAjrSgAsTY0Vf4E4log7ob5gAVqMwicB2IeMsObE4gbgfgX1Kx45EjmBeILUIltQMxMQcS6Q835AMSyyBLSQPwYKjmTwtQzG2pOG7qELhB/hEqWUGCBLdSMXdgkXaDheIkCC0Dx+A+UaVmwpIg9QGwIxL8oKIH4gJgRiD+x4FBwlcIizhRKn6FFfcAMxDVQ9mEGapY70DzVD43gi0DMRi2DFYA4FIjPQg0HFT/6yBktE4h/U6lMOg9N7mCzWZBKxB/QGo2BzNL0DBAfBeK5QPwbJsk45FsVAAEGAKjA0W/LnV8HAAAAAElFTkSuQmCC"></img></a>`
                $('.int_msg_in_con').last().append(text_btn_response);
            }

            //creating image function
            function bot_image_res(image) {
                //$(".int_bot_box").toggle();
                text_str_response = `<p class="res"><img src="${image}" alt="img" width="200" height="200"></p></div>`
                $('.int_bot_conversation').last().append(str_int_msg_out);
                $('.int_msg_out').last().append(str_int_msg_in_con);
                $('.int_msg_in_con').last().append(text_str_response);
                $('.int_msg_in_con').last().after(str_clr);
                chatbot_container_auto_scroll()
            }

            //creating video function
            function bot_video_res(video_src) {
                if (video_src.includes('youtube')){
                    text_str_response = `<p class="res"><iframe width="300" height="200" src="${video_src}"?></iframe></p></div>`
                    $('.int_bot_conversation').last().append(str_int_msg_out);
                    $('.int_msg_out').last().append(str_int_msg_in_con);
                    $('.int_msg_in_con').last().append(text_str_response);
                    $('.int_msg_in_con').last().after(str_clr);
                }else {
                    text_str_response = `<p class="res"><video width="230" height="270" controls>
                    <source src="${video_src}" type="video/mp4"></video></p></div>`
                    $('.int_bot_conversation').last().append(str_int_msg_out);
                    $('.int_msg_out').last().append(str_int_msg_in_con);
                    $('.int_msg_in_con').last().append(text_str_response);
                    $('.int_msg_in_con').last().after(str_clr);
                }
                chatbot_container_auto_scroll()
            }

            //creating pdf function
            function bot_pdf_res(pdf_src) {
                $('.int_bot_conversation').last().append(str_int_msg_out);
                $('.int_msg_out').last().append(str_int_msg_in_con);
                text_str_response = `<a class="int_msg_option pdf_res" href="${pdf_src}" target="_blank"><img width="75" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iCiAgIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiCiAgIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIKICAgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIgogICB4bWxuczpzb2RpcG9kaT0iaHR0cDovL3NvZGlwb2RpLnNvdXJjZWZvcmdlLm5ldC9EVEQvc29kaXBvZGktMC5kdGQiCiAgIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIgogICB3aWR0aD0iNzUuMzIwMTI5bW0iCiAgIGhlaWdodD0iOTIuNjA0MTY0bW0iCiAgIHZpZXdCb3g9IjAgMCA3NS4zMjAxMjkgOTIuNjA0MTY0IgogICB2ZXJzaW9uPSIxLjEiCiAgIGlkPSJzdmc4IgogICBpbmtzY2FwZTp2ZXJzaW9uPSIwLjkyLjMgKDI0MDU1NDYsIDIwMTgtMDMtMTEpIgogICBzb2RpcG9kaTpkb2NuYW1lPSJQREYgZmlsZSBpY29uLnN2ZyI+CiAgPHRpdGxlCiAgICAgaWQ9InRpdGxlMjY4MiI+UERGIGZpbGUgaWNvbjwvdGl0bGU+CiAgPGRlZnMKICAgICBpZD0iZGVmczIiPgogICAgPGNsaXBQYXRoCiAgICAgICBjbGlwUGF0aFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIKICAgICAgIGlkPSJjbGlwUGF0aDgzOSI+CiAgICAgIDxyZWN0CiAgICAgICAgIHN0eWxlPSJvcGFjaXR5OjE7ZmlsbDojMDAwMDAwO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoxO3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjpmaWxsIG1hcmtlcnMgc3Ryb2tlIgogICAgICAgICBpZD0icmVjdDg0MSIKICAgICAgICAgd2lkdGg9IjY4LjY4ODI2MyIKICAgICAgICAgaGVpZ2h0PSI2Ny44ODY0NTkiCiAgICAgICAgIHg9Ii03NC4wMzM2NjEiCiAgICAgICAgIHk9IjE0MS40NDkxMyIgLz4KICAgIDwvY2xpcFBhdGg+CiAgPC9kZWZzPgogIDxzb2RpcG9kaTpuYW1lZHZpZXcKICAgICBpZD0iYmFzZSIKICAgICBwYWdlY29sb3I9IiNmZmZmZmYiCiAgICAgYm9yZGVyY29sb3I9IiM2NjY2NjYiCiAgICAgYm9yZGVyb3BhY2l0eT0iMS4wIgogICAgIGlua3NjYXBlOnBhZ2VvcGFjaXR5PSIwLjAiCiAgICAgaW5rc2NhcGU6cGFnZXNoYWRvdz0iMiIKICAgICBpbmtzY2FwZTp6b29tPSIwLjk4OTk0OTQ5IgogICAgIGlua3NjYXBlOmN4PSIxMjIuMTI3MjciCiAgICAgaW5rc2NhcGU6Y3k9IjE2Ny4wMjgyMyIKICAgICBpbmtzY2FwZTpkb2N1bWVudC11bml0cz0ibW0iCiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0iZzg5OSIKICAgICBzaG93Z3JpZD0iZmFsc2UiCiAgICAgaW5rc2NhcGU6b2JqZWN0LW5vZGVzPSJmYWxzZSIKICAgICBpbmtzY2FwZTp3aW5kb3ctd2lkdGg9IjEzNjYiCiAgICAgaW5rc2NhcGU6d2luZG93LWhlaWdodD0iNzA1IgogICAgIGlua3NjYXBlOndpbmRvdy14PSItOCIKICAgICBpbmtzY2FwZTp3aW5kb3cteT0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LW1heGltaXplZD0iMSIKICAgICBpbmtzY2FwZTpzbmFwLXBhZ2U9ImZhbHNlIgogICAgIGlua3NjYXBlOnNuYXAtb3RoZXJzPSJmYWxzZSIKICAgICBmaXQtbWFyZ2luLXRvcD0iMCIKICAgICBmaXQtbWFyZ2luLWxlZnQ9IjAiCiAgICAgZml0LW1hcmdpbi1yaWdodD0iMCIKICAgICBmaXQtbWFyZ2luLWJvdHRvbT0iMCIgLz4KICA8bWV0YWRhdGEKICAgICBpZD0ibWV0YWRhdGE1Ij4KICAgIDxyZGY6UkRGPgogICAgICA8Y2M6V29yawogICAgICAgICByZGY6YWJvdXQ9IiI+CiAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+CiAgICAgICAgPGRjOnR5cGUKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIiAvPgogICAgICAgIDxkYzp0aXRsZT5QREYgZmlsZSBpY29uPC9kYzp0aXRsZT4KICAgICAgICA8ZGM6ZGF0ZT4wOC8xMC8yMDE4PC9kYzpkYXRlPgogICAgICAgIDxkYzpjcmVhdG9yPgogICAgICAgICAgPGNjOkFnZW50PgogICAgICAgICAgICA8ZGM6dGl0bGU+QWRvYmUgU3lzdGVtczwvZGM6dGl0bGU+CiAgICAgICAgICA8L2NjOkFnZW50PgogICAgICAgIDwvZGM6Y3JlYXRvcj4KICAgICAgICA8ZGM6cHVibGlzaGVyPgogICAgICAgICAgPGNjOkFnZW50PgogICAgICAgICAgICA8ZGM6dGl0bGU+Q01ldGFsQ29yZTwvZGM6dGl0bGU+CiAgICAgICAgICA8L2NjOkFnZW50PgogICAgICAgIDwvZGM6cHVibGlzaGVyPgogICAgICAgIDxkYzpkZXNjcmlwdGlvbj5GdWVudGUgZGVsIHRleHRvICZxdW90O1BERiZxdW90OzoKRnJhbmtsaW4gR290aGljIE1lZGl1bSBDb25kPC9kYzpkZXNjcmlwdGlvbj4KICAgICAgICA8Y2M6bGljZW5zZQogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbGljZW5zZXMvYnktc2EvNC4wLyIgLz4KICAgICAgPC9jYzpXb3JrPgogICAgICA8Y2M6TGljZW5zZQogICAgICAgICByZGY6YWJvdXQ9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL2xpY2Vuc2VzL2J5LXNhLzQuMC8iPgogICAgICAgIDxjYzpwZXJtaXRzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNSZXByb2R1Y3Rpb24iIC8+CiAgICAgICAgPGNjOnBlcm1pdHMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI0Rpc3RyaWJ1dGlvbiIgLz4KICAgICAgICA8Y2M6cmVxdWlyZXMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI05vdGljZSIgLz4KICAgICAgICA8Y2M6cmVxdWlyZXMKICAgICAgICAgICByZGY6cmVzb3VyY2U9Imh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL25zI0F0dHJpYnV0aW9uIiAvPgogICAgICAgIDxjYzpwZXJtaXRzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNEZXJpdmF0aXZlV29ya3MiIC8+CiAgICAgICAgPGNjOnJlcXVpcmVzCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyNTaGFyZUFsaWtlIiAvPgogICAgICA8L2NjOkxpY2Vuc2U+CiAgICA8L3JkZjpSREY+CiAgPC9tZXRhZGF0YT4KICA8ZwogICAgIGlua3NjYXBlOmxhYmVsPSJDYXBhIDEiCiAgICAgaW5rc2NhcGU6Z3JvdXBtb2RlPSJsYXllciIKICAgICBpZD0ibGF5ZXIxIgogICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKDM2LjA3NjE3MiwtOTMuNzMxNzc0KSI+CiAgICA8ZwogICAgICAgaWQ9Imc4OTkiCiAgICAgICB0cmFuc2Zvcm09Im1hdHJpeCgxLjQ4NDMwNTQsMCwwLDEuNDg0MzA1NCwxNy40NzE4ODUsLTkwLjI0MzUwMikiPgogICAgICA8ZwogICAgICAgICBpZD0iZzg3NiI+CiAgICAgICAgPHBhdGgKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICAgIHN0eWxlPSJjb2xvcjojMDAwMDAwO2ZvbnQtc3R5bGU6bm9ybWFsO2ZvbnQtdmFyaWFudDpub3JtYWw7Zm9udC13ZWlnaHQ6bm9ybWFsO2ZvbnQtc3RyZXRjaDpub3JtYWw7Zm9udC1zaXplOm1lZGl1bTtsaW5lLWhlaWdodDpub3JtYWw7Zm9udC1mYW1pbHk6c2Fucy1zZXJpZjtmb250LXZhcmlhbnQtbGlnYXR1cmVzOm5vcm1hbDtmb250LXZhcmlhbnQtcG9zaXRpb246bm9ybWFsO2ZvbnQtdmFyaWFudC1jYXBzOm5vcm1hbDtmb250LXZhcmlhbnQtbnVtZXJpYzpub3JtYWw7Zm9udC12YXJpYW50LWFsdGVybmF0ZXM6bm9ybWFsO2ZvbnQtZmVhdHVyZS1zZXR0aW5nczpub3JtYWw7dGV4dC1pbmRlbnQ6MDt0ZXh0LWFsaWduOnN0YXJ0O3RleHQtZGVjb3JhdGlvbjpub25lO3RleHQtZGVjb3JhdGlvbi1saW5lOm5vbmU7dGV4dC1kZWNvcmF0aW9uLXN0eWxlOnNvbGlkO3RleHQtZGVjb3JhdGlvbi1jb2xvcjojMDAwMDAwO2xldHRlci1zcGFjaW5nOm5vcm1hbDt3b3JkLXNwYWNpbmc6bm9ybWFsO3RleHQtdHJhbnNmb3JtOm5vbmU7d3JpdGluZy1tb2RlOmxyLXRiO2RpcmVjdGlvbjpsdHI7dGV4dC1vcmllbnRhdGlvbjptaXhlZDtkb21pbmFudC1iYXNlbGluZTphdXRvO2Jhc2VsaW5lLXNoaWZ0OmJhc2VsaW5lO3RleHQtYW5jaG9yOnN0YXJ0O3doaXRlLXNwYWNlOm5vcm1hbDtzaGFwZS1wYWRkaW5nOjA7Y2xpcC1ydWxlOm5vbnplcm87ZGlzcGxheTppbmxpbmU7b3ZlcmZsb3c6dmlzaWJsZTt2aXNpYmlsaXR5OnZpc2libGU7b3BhY2l0eToxO2lzb2xhdGlvbjphdXRvO21peC1ibGVuZC1tb2RlOm5vcm1hbDtjb2xvci1pbnRlcnBvbGF0aW9uOnNSR0I7Y29sb3ItaW50ZXJwb2xhdGlvbi1maWx0ZXJzOmxpbmVhclJHQjtzb2xpZC1jb2xvcjojMDAwMDAwO3NvbGlkLW9wYWNpdHk6MTt2ZWN0b3ItZWZmZWN0Om5vbmU7ZmlsbDojZmYyMTE2O2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDoyLjExNjY2NjU2O3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZTtzdHJva2UtZGFzaG9mZnNldDowO3N0cm9rZS1vcGFjaXR5OjE7cGFpbnQtb3JkZXI6bWFya2VycyBmaWxsIHN0cm9rZTtjb2xvci1yZW5kZXJpbmc6YXV0bztpbWFnZS1yZW5kZXJpbmc6YXV0bztzaGFwZS1yZW5kZXJpbmc6YXV0bzt0ZXh0LXJlbmRlcmluZzphdXRvO2VuYWJsZS1iYWNrZ3JvdW5kOmFjY3VtdWxhdGUiCiAgICAgICAgICAgZD0ibSAtMjkuNjMyODEyLDEyMy45NDcyNyBjIC0zLjU1MTk2NywwIC02LjQ0MzM2LDIuODkzNDcgLTYuNDQzMzYsNi40NDUzMSB2IDQ5LjQ5ODA0IGMgMCwzLjU1MTg1IDIuODkxMzkzLDYuNDQ1MzIgNi40NDMzNiw2LjQ0NTMyIEggOC4yMTY3OTY5IGMgMy41NTE5NjYxLDAgNi40NDMzNTkxLC0yLjg5MzM1IDYuNDQzMzU5MSwtNi40NDUzMiB2IC00MC43MDExNyBjIDAsMCAwLjEwMTM1MywtMS4xOTE4MSAtMC40MTYwMTUsLTIuMzUxNTYgLTAuNDg0OTY5LC0xLjA4NzExIC0xLjI3NTM5MSwtMS44NDM3NSAtMS4yNzUzOTEsLTEuODQzNzUgYSAxLjA1ODQzOTEsMS4wNTg0MzkxIDAgMCAwIC0wLjAwNTksLTAuMDA4IEwgMy41NzIyMjQ2LDEyNS43NzUyIGEgMS4wNTg0MzkxLDEuMDU4NDM5MSAwIDAgMCAtMC4wMTU2MjUsLTAuMDE1NiBjIDAsMCAtMC44MDE3MzkyLC0wLjc2MzQ0IC0xLjk5MDIzNDQsLTEuMjczNDQgLTEuMzk5Mzk1NTIsLTAuNjAwNSAtMi44NDE3OTY4LC0wLjUzNzExIC0yLjg0MTc5NjgsLTAuNTM3MTEgbCAwLjAyMTQ4NCwtMC4wMDIgeiIKICAgICAgICAgICBpZD0icGF0aDg5MCIgLz4KICAgICAgICA8cGF0aAogICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgc3R5bGU9ImNvbG9yOiMwMDAwMDA7Zm9udC1zdHlsZTpub3JtYWw7Zm9udC12YXJpYW50Om5vcm1hbDtmb250LXdlaWdodDpub3JtYWw7Zm9udC1zdHJldGNoOm5vcm1hbDtmb250LXNpemU6bWVkaXVtO2xpbmUtaGVpZ2h0Om5vcm1hbDtmb250LWZhbWlseTpzYW5zLXNlcmlmO2ZvbnQtdmFyaWFudC1saWdhdHVyZXM6bm9ybWFsO2ZvbnQtdmFyaWFudC1wb3NpdGlvbjpub3JtYWw7Zm9udC12YXJpYW50LWNhcHM6bm9ybWFsO2ZvbnQtdmFyaWFudC1udW1lcmljOm5vcm1hbDtmb250LXZhcmlhbnQtYWx0ZXJuYXRlczpub3JtYWw7Zm9udC1mZWF0dXJlLXNldHRpbmdzOm5vcm1hbDt0ZXh0LWluZGVudDowO3RleHQtYWxpZ246c3RhcnQ7dGV4dC1kZWNvcmF0aW9uOm5vbmU7dGV4dC1kZWNvcmF0aW9uLWxpbmU6bm9uZTt0ZXh0LWRlY29yYXRpb24tc3R5bGU6c29saWQ7dGV4dC1kZWNvcmF0aW9uLWNvbG9yOiMwMDAwMDA7bGV0dGVyLXNwYWNpbmc6bm9ybWFsO3dvcmQtc3BhY2luZzpub3JtYWw7dGV4dC10cmFuc2Zvcm06bm9uZTt3cml0aW5nLW1vZGU6bHItdGI7ZGlyZWN0aW9uOmx0cjt0ZXh0LW9yaWVudGF0aW9uOm1peGVkO2RvbWluYW50LWJhc2VsaW5lOmF1dG87YmFzZWxpbmUtc2hpZnQ6YmFzZWxpbmU7dGV4dC1hbmNob3I6c3RhcnQ7d2hpdGUtc3BhY2U6bm9ybWFsO3NoYXBlLXBhZGRpbmc6MDtjbGlwLXJ1bGU6bm9uemVybztkaXNwbGF5OmlubGluZTtvdmVyZmxvdzp2aXNpYmxlO3Zpc2liaWxpdHk6dmlzaWJsZTtvcGFjaXR5OjE7aXNvbGF0aW9uOmF1dG87bWl4LWJsZW5kLW1vZGU6bm9ybWFsO2NvbG9yLWludGVycG9sYXRpb246c1JHQjtjb2xvci1pbnRlcnBvbGF0aW9uLWZpbHRlcnM6bGluZWFyUkdCO3NvbGlkLWNvbG9yOiMwMDAwMDA7c29saWQtb3BhY2l0eToxO3ZlY3Rvci1lZmZlY3Q6bm9uZTtmaWxsOiNmNWY1ZjU7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOm5vbnplcm87c3Ryb2tlOm5vbmU7c3Ryb2tlLXdpZHRoOjIuMTE2NjY2NTY7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lO3N0cm9rZS1kYXNob2Zmc2V0OjA7c3Ryb2tlLW9wYWNpdHk6MTtwYWludC1vcmRlcjptYXJrZXJzIGZpbGwgc3Ryb2tlO2NvbG9yLXJlbmRlcmluZzphdXRvO2ltYWdlLXJlbmRlcmluZzphdXRvO3NoYXBlLXJlbmRlcmluZzphdXRvO3RleHQtcmVuZGVyaW5nOmF1dG87ZW5hYmxlLWJhY2tncm91bmQ6YWNjdW11bGF0ZSIKICAgICAgICAgICBkPSJtIC0yOS42MzI4MTIsMTI2LjA2NDQ1IGggMjguMzc4OTA1OCBhIDEuMDU4NDM5MSwxLjA1ODQzOTEgMCAwIDAgMC4wMjE0ODQsMCBjIDAsMCAxLjEzNDgwNDQ4LDAuMDExIDEuOTY0ODQzNzgsMC4zNjcxOSAwLjc5ODg5NzcyLDAuMzQyODIgMS4zNjUzNjk4MiwwLjg2MTc2IDEuMzY5MTQwNjIsMC44NjUyNCAxLjI1ZS01LDFlLTUgMC4wMDM5MSwwLjAwNCAwLjAwMzkxLDAuMDA0IGwgOS4zNjcxODY4LDkuMTg5NDUgYyAwLDAgMC41NjQzNTQsMC41OTU4MiAwLjgzNzg5MSwxLjIwODk5IDAuMjIwNzc5LDAuNDk0OTEgMC4yMzQzNzUsMS40MDAzOSAwLjIzNDM3NSwxLjQwMDM5IGEgMS4wNTg0MzkxLDEuMDU4NDM5MSAwIDAgMCAtMC4wMDIsMC4wNDQ5IHYgNDAuNzQ2MDkgYyAwLDIuNDE1OTIgLTEuOTEwMjU4LDQuMzI4MTMgLTQuMzI2MTcxNyw0LjMyODEzIEggLTI5LjYzMjgxMiBjIC0yLjQxNTkxNCwwIC00LjMyNjE3MiwtMS45MTIwOSAtNC4zMjYxNzIsLTQuMzI4MTMgdiAtNDkuNDk4MDQgYyAwLC0yLjQxNjAzIDEuOTEwMjU4LC00LjMyODEzIDQuMzI2MTcyLC00LjMyODEzIHoiCiAgICAgICAgICAgaWQ9InJlY3QyNjg0IiAvPgogICAgICAgIDxwYXRoCiAgICAgICAgICAgc29kaXBvZGk6bm9kZXR5cGVzPSJzY2Njc2NjY3NjY2NjY3Nzc2NhY2Njc2NjY2NjY2NjY2NjY2NjY2NjY2NjYWNjY2NjY2NjYyIKICAgICAgICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgICAgICAgIGQ9Im0gLTIzLjQwNzY2LDE2MS4wOTI5OSBjIC0xLjQ1NjY5LC0xLjQ1NjY5IDAuMTE5MzQsLTMuNDU4MzkgNC4zOTY0OCwtNS41ODM5NyBsIDIuNjkxMjQsLTEuMzM3NDMgMS4wNDg0NSwtMi4yOTM5OSBjIDAuNTc2NjUsLTEuMjYxNjkgMS40MzcyOSwtMy4zMjAzNiAxLjkxMjU0LC00LjU3NDggbCAwLjg2NDEsLTIuMjgwODIgLTAuNTk1NDYsLTEuNjg3OTMgYyAtMC43MzIxNywtMi4wNzU0NyAtMC45OTMyNiwtNS4xOTQzOCAtMC41Mjg3MiwtNi4zMTU4OCAwLjYyOTIzLC0xLjUxOTA5IDIuNjkwMjksLTEuMzYzMjMgMy41MDYyNiwwLjI2NTE1IDAuNjM3MjcsMS4yNzE3NiAwLjU3MjEyLDMuNTc0ODggLTAuMTgzMjksNi40Nzk0NiBsIC0wLjYxOTMsMi4zODEyNSAwLjU0NTUsMC45MjYwNCBjIDAuMzAwMDMsMC41MDkzMiAxLjE3NjQsMS43MTg2NyAxLjk0NzUsMi42ODc0MyBsIDEuNDQ5MjQsMS44MDI3MiAxLjgwMzM3MjgsLTAuMjM1MzMgYyA1LjcyOTAwMzk5LC0wLjc0NzU4IDcuNjkxMjQ3MiwwLjUyMyA3LjY5MTI0NzIsMi4zNDQ3NiAwLDIuMjk5MjEgLTQuNDk4NDkxNCwyLjQ4ODk5IC04LjI3NjA4NjUsLTAuMTY0MjMgLTAuODQ5OTY2NiwtMC41OTY5OCAtMS40MzM2NjA1LC0xLjE5MDAxIC0xLjQzMzY2MDUsLTEuMTkwMDEgMCwwIC0yLjM2NjUzMjYsMC40ODE3OCAtMy41MzE3MDQsMC43OTU4MyAtMS4yMDI3MDcsMC4zMjQxNyAtMS44MDI3NCwwLjUyNzE5IC0zLjU2NDUwOSwxLjEyMTg2IDAsMCAtMC42MTgxNCwwLjg5NzY3IC0xLjAyMDk0LDEuNTUwMjYgLTEuNDk4NTgsMi40Mjc5IC0zLjI0ODMzLDQuNDM5OTggLTQuNDk3OTMsNS4xNzIzIC0xLjM5OTEsMC44MTk5MyAtMi44NjU4NCwwLjg3NTgyIC0zLjYwNDMzLDAuMTM3MzMgeiBtIDIuMjg2MDUsLTAuODE2NjggYyAwLjgxODgzLC0wLjUwNjA3IDIuNDc2MTYsLTIuNDY2MjUgMy42MjM0MSwtNC4yODU1MyBsIDAuNDY0NDksLTAuNzM2NTggLTIuMTE0OTcsMS4wNjMzOSBjIC0zLjI2NjU1LDEuNjQyMzkgLTQuNzYwOTMsMy4xOTAzMyAtMy45ODM4Niw0LjEyNjY0IDAuNDM2NTMsMC41MjU5OCAwLjk1ODc0LDAuNDgyMzcgMi4wMTA5MywtMC4xNjc5MiB6IG0gMjEuMjE4MDksLTUuOTU1NzggYyAwLjgwMDg5LC0wLjU2MDk3IDAuNjg0NjMsLTEuNjkxNDIgLTAuMjIwODIsLTIuMTQ3MiAtMC43MDQ2NiwtMC4zNTQ3MSAtMS4yNzI2MDc0LC0wLjQyNzU5IC0zLjEwMzE1NzQsLTAuNDAwNTcgLTEuMTI0OSwwLjA3NjcgLTIuOTMzNzY0NywwLjMwMzQgLTMuMjQwMzM0NywwLjM3MjM3IDAsMCAwLjk5MzcxNiwwLjY4Njc4IDEuNDM0ODk2LDAuOTM5MjIgMC41ODczMSwwLjMzNTQ0IDIuMDE0NTE2MSwwLjk1ODExIDMuMDU2NTE2MSwxLjI3NzA2IDEuMDI3ODUsMC4zMTQ2MSAxLjYyMjQsMC4yODE0NCAyLjA3MjksLTAuMDQwOSB6IG0gLTguNTMxNTIsLTMuNTQ1OTQgYyAtMC40ODQ3LC0wLjUwOTUyIC0xLjMwODg5LC0xLjU3Mjk2IC0xLjgzMTUyLC0yLjM2MzIgLTAuNjgzNTMsLTAuODk2NDMgLTEuMDI2MjksLTEuNTI4ODcgLTEuMDI2MjksLTEuNTI4ODcgMCwwIC0wLjQ5OTYsMS42MDY5NCAtMC45MDk0OCwyLjU3Mzk0IGwgLTEuMjc4NzYsMy4xNjA3NiAtMC4zNzA3NSwwLjcxNjk1IGMgMCwwIDEuOTcxMDQzLC0wLjY0NjI3IDIuOTczODksLTAuOTA4MjIgMS4wNjIxNjY4LC0wLjI3NzQ0IDMuMjE3ODcsLTAuNzAxMzQgMy4yMTc4NywtMC43MDEzNCB6IG0gLTIuNzQ5MzgsLTExLjAyNTczIGMgMC4xMjM2MywtMS4wMzc1IDAuMTc2MSwtMi4wNzM0NiAtMC4xNTcyNCwtMi41OTU4NyAtMC45MjQ2LC0xLjAxMDc3IC0yLjA0MDU3LC0wLjE2Nzg3IC0xLjg1MTU0LDIuMjM1MTcgMC4wNjM2LDAuODA4NCAwLjI2NDQzLDIuMTkwMzMgMC41MzI5MiwzLjA0MjA5IGwgMC40ODgxNywxLjU0ODYzIDAuMzQzNTgsLTEuMTY2MzggYyAwLjE4ODk3LC0wLjY0MTUxIDAuNDc4ODIsLTIuMDIwMTUgMC42NDQxMSwtMy4wNjM2NCB6IgogICAgICAgICAgIHN0eWxlPSJmaWxsOiNmZjIxMTY7ZmlsbC1vcGFjaXR5OjE7c3Ryb2tlLXdpZHRoOjAuMjY0NTgzMzUiCiAgICAgICAgICAgaWQ9InBhdGgyNjk3IiAvPgogICAgICAgIDxnCiAgICAgICAgICAgaWQ9Imc4NTgiPgogICAgICAgICAgPHBhdGgKICAgICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgICBpZD0icGF0aDg0NSIKICAgICAgICAgICAgIHN0eWxlPSJmb250LXN0eWxlOm5vcm1hbDtmb250LXZhcmlhbnQ6bm9ybWFsO2ZvbnQtd2VpZ2h0Om5vcm1hbDtmb250LXN0cmV0Y2g6bm9ybWFsO2ZvbnQtc2l6ZTptZWRpdW07bGluZS1oZWlnaHQ6MTI1JTtmb250LWZhbWlseTonRnJhbmtsaW4gR290aGljIE1lZGl1bSBDb25kJzstaW5rc2NhcGUtZm9udC1zcGVjaWZpY2F0aW9uOidGcmFua2xpbiBHb3RoaWMgTWVkaXVtIENvbmQnO2xldHRlci1zcGFjaW5nOjBweDt3b3JkLXNwYWNpbmc6NC4yNjAwMDAyM3B4O2ZpbGw6IzJjMmMyYztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MC4zNTgyNDY1NnB4O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgICBkPSJtIC0yMC45MzA0MjMsMTY3LjgzODYyIGggMi4zNjQ5ODYgcSAxLjEzMzUxNCwwIDEuODQwMjEzLDAuMjE2OSAwLjcwNjY5OCwwLjIwOTkxIDEuMTg5NDg5LDAuOTQ0NiAwLjQ4Mjc5NSwwLjcyNzY5IDAuNDgyNzk1LDEuNzU2MjUgMCwwLjk0NDU5IC0wLjM5MTgzMiwxLjYyMzMgLTAuMzkxODMzLDAuNjc4NzEgLTEuMDU2NTQ4LDAuOTc5NTggLTAuNjU3NzIsMC4zMDA4NyAtMi4wMjkxMywwLjMwMDg3IGggLTAuODE4NjUxIHYgMy43Mjk0MSBoIC0xLjU4MTMyMiB6IG0gMS41ODEzMjIsMS4yMjQ0NyB2IDMuMzMwNTggaCAwLjc4MzY2NCBxIDEuMDQ5NTUyLDAgMS40NDgzOCwtMC4zOTE4NCAwLjQwNTgyNiwtMC4zOTE4MyAwLjQwNTgyNiwtMS4yNzM0NSAwLC0wLjY1NzcyIC0wLjI2NTg4NywtMS4wNjM1NSAtMC4yNjU4ODQsLTAuNDEyODIgLTAuNTg3NzQ3LC0wLjUwMzc4IC0wLjMxNDg2NiwtMC4wOTggLTEuMDAwNTcyLC0wLjA5OCB6IiAvPgogICAgICAgICAgPHBhdGgKICAgICAgICAgICAgIGlua3NjYXBlOmNvbm5lY3Rvci1jdXJ2YXR1cmU9IjAiCiAgICAgICAgICAgICBpZD0icGF0aDg0NyIKICAgICAgICAgICAgIHN0eWxlPSJmb250LXN0eWxlOm5vcm1hbDtmb250LXZhcmlhbnQ6bm9ybWFsO2ZvbnQtd2VpZ2h0Om5vcm1hbDtmb250LXN0cmV0Y2g6bm9ybWFsO2ZvbnQtc2l6ZTptZWRpdW07bGluZS1oZWlnaHQ6MTI1JTtmb250LWZhbWlseTonRnJhbmtsaW4gR290aGljIE1lZGl1bSBDb25kJzstaW5rc2NhcGUtZm9udC1zcGVjaWZpY2F0aW9uOidGcmFua2xpbiBHb3RoaWMgTWVkaXVtIENvbmQnO2xldHRlci1zcGFjaW5nOjBweDt3b3JkLXNwYWNpbmc6NC4yNjAwMDAyM3B4O2ZpbGw6IzJjMmMyYztmaWxsLW9wYWNpdHk6MTtzdHJva2U6bm9uZTtzdHJva2Utd2lkdGg6MC4zNTgyNDY1NnB4O3N0cm9rZS1saW5lY2FwOmJ1dHQ7c3Ryb2tlLWxpbmVqb2luOm1pdGVyO3N0cm9rZS1vcGFjaXR5OjEiCiAgICAgICAgICAgICBkPSJtIC0xMy44NDI0NjEsMTY3LjgzODYyIGggMi4xNDgwODIgcSAxLjU2MDMzMywwIDIuNDkwOTMxOCwwLjU1Mjc2IDAuOTM3NTk5MywwLjU1Mjc2IDEuNDEzMzk3MywxLjY0NDMgMC40ODI3OTEsMS4wOTE1MyAwLjQ4Mjc5MSwyLjQyMDk2IDAsMS4zOTk0IC0wLjQzMzgxNTEsMi40OTc5MyAtMC40MjY4MTQ5LDEuMDkxNTMgLTEuMzE1NDM0OCwxLjc2MzI0IC0wLjg4MTYyMzMsMC42NzE3MiAtMi41MTg5MjEyLDAuNjcxNzIgaCAtMi4yNjcwMzEgeiBtIDEuNTgxMzI2LDEuMjY2NDUgdiA3LjAxOCBoIDAuNjU3NzE1IHEgMS4zNzg0MTEsMCAyLjAwMTE0NCwtMC45NTE2IDAuNjIyNzMyOSwtMC45NTg1OCAwLjYyMjczMjksLTIuNTUzOSAwLC0zLjUxMjUgLTIuNjIzODc2OSwtMy41MTI1IHoiIC8+CiAgICAgICAgICA8cGF0aAogICAgICAgICAgICAgaW5rc2NhcGU6Y29ubmVjdG9yLWN1cnZhdHVyZT0iMCIKICAgICAgICAgICAgIGlkPSJwYXRoODQ5IgogICAgICAgICAgICAgc3R5bGU9ImZvbnQtc3R5bGU6bm9ybWFsO2ZvbnQtdmFyaWFudDpub3JtYWw7Zm9udC13ZWlnaHQ6bm9ybWFsO2ZvbnQtc3RyZXRjaDpub3JtYWw7Zm9udC1zaXplOm1lZGl1bTtsaW5lLWhlaWdodDoxMjUlO2ZvbnQtZmFtaWx5OidGcmFua2xpbiBHb3RoaWMgTWVkaXVtIENvbmQnOy1pbmtzY2FwZS1mb250LXNwZWNpZmljYXRpb246J0ZyYW5rbGluIEdvdGhpYyBNZWRpdW0gQ29uZCc7bGV0dGVyLXNwYWNpbmc6MHB4O3dvcmQtc3BhY2luZzo0LjI2MDAwMDIzcHg7ZmlsbDojMmMyYzJjO2ZpbGwtb3BhY2l0eToxO3N0cm9rZTpub25lO3N0cm9rZS13aWR0aDowLjM1ODI0NjU2cHg7c3Ryb2tlLWxpbmVjYXA6YnV0dDtzdHJva2UtbGluZWpvaW46bWl0ZXI7c3Ryb2tlLW9wYWNpdHk6MSIKICAgICAgICAgICAgIGQ9Im0gLTUuNzg4OTA5NiwxNjcuODM4NjIgaCA1LjMwMzcyOTQxIHYgMS4yNjY0NSBIIC00LjIwNzU4NDIgdiAyLjg1NDc4IGggMi45ODA3MjI1IHYgMS4yNjY0NiBoIC0yLjk4MDcyMjUgdiA0LjE2MzIyIGggLTEuNTgxMzI1NCB6IiAvPgogICAgICAgIDwvZz4KICAgICAgPC9nPgogICAgPC9nPgogIDwvZz4KPC9zdmc+Cg=="></img></a>`
                $('.int_msg_in_con').last().append(text_str_response);
                $('.int_msg_in_con').last().after(str_clr);
                chatbot_container_auto_scroll()
            }

            function bot_location_res(location_src) {
                $('.int_bot_conversation').last().append(str_int_msg_out);
                $('.int_msg_out').last().append(str_int_msg_in_con);
                text_str_response = `<a class="int_msg_option loc_res" href="${location_src}" target="_blank"><img width="75" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAIACAYAAAD0eNT6AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAOEAAADhAB+dBQOQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAFIeSURBVHja7Z15kFTV2f9/QUNpNMaqWBX/oFKxrFQqf5lKKlWpipUXiYkmb6K+6oCKbAqKuIEa3A2gIIgisi8ii+wMwzLs+zoMDMPOALMw+9rdM9Mz07N3n985PRcdcJbunnv7bp9jfapSiYHuc5/zfL997jnP8/+EEP8PAKzN9Ny1t0r6SO6T9JMkSEZK3pdMkyyVJEqSJTslByTHJOmSC5IsSb6kTFIlCUhaNQLaf1em/TtZ2v8nXfszDmh/ZrL2dyzV/s73tc+QoH2m+7TPeCvPDMD6MAkA5gp7b8m9kgclwyUTJSskeyVnJIWSeomwGfXaZz+jfZcV2ncbrn1X9Z17EwMAGAAApwp8L8mvJH0lwyTjJcskhzSBDNpQ3PUiqM3BIW1Oxmtz1Febs17EEAAGAMAOYn+XthX+umSRJM2mv96ttIuQps3l69rc3kWsAWAAAMzctlfvuwdJpkp2SEoQ7LhRos35VO0Z3MfrBAAMAIARgq/eVw+WLJCcl7QgwpajRXs2C7RndS+xC4ABAIhG7G+W/FEyWjvxXoq42pZS7RmO1p7pzcQ4AAYA4Jrg3yF5SDJBsk+7Eod4OpOA9ownaM/8DtYAYAAA3HUq/0/aifPjLj+F73aCWgyM12KCWweAAQBwmOjfLRkiWSXxIXzQCT4tRlSs3M3aAQwAgD3f4/9FMklyShJC3CBKQlrsTNJiifMDgAEAsKjo/0z75bZe4kfAQGf8WmypGPsZaw4wAADmiv5PJQMlmyVNiBTEiSYt5lTs/ZS1CBgAgPiI/m2SAZIkSQNiBCbToMWiisnbWKOAAQDQV/RVJ7wnJGu5pgcWv2a4VotVOiICBgCgB9f1HpaslNQiLmAzarXYfZjrhYABAIhM+FU/+Y+0vvQICTiBfC2m+7DGAQMAcL3o3yR5RJIsaUUwwKG0ajGuYv0m1j5gAMDNwn+P5BNJMeIALqNYi/17yAWAAQC3iP6PJU9KdlGgByC8BnZpa+LH5AjAAIAThf8u7T1oOUkfoEPKtTVyFzkDMADgBOH/tWSupJ4EDxAR9dqa+TU5BDAAYEfhv1+ygW57AD3qVqjW0P3kFMAAgB1O86t3makkbwBdSdXWFrcHAAMAlivP+6okh0QNYChXtbVG2WHAAICpwn+HdmipksQMEFcqtbV3B7kIMAAQ71/8b0t8JGIAU/Fpa5EdAcAAgKHCf4tkDFf5ACx5hVCtzVvIVYABAD2Fv7dkFBX7AGxRYVCt1d7kLsAAQE+E/2bJcBrzANiyAZFauzeTywADANEIv2rFO0iSTSIFsDXZ2lqmJTFgAKBb8e8rOUPiBHAUak33JccBBgA668y3nkQJ4GjW04EQMABwTfhvl0ySNJIcAVxBo7bmbycHYgDAncL/I8lQSSkJEcCVlGo54EfkRAwAuEf8/yxJIwECgJYL/kxuxACAs4X/l5JVJDwA6ACVG35JrsQAgPO69L0lCZDkAKALAlquoOsgBgAcIP6/k5wksQFAFKic8TtyKAYA7Fu3f7KkhWQGADHQouUQ+gtgAMBmxXwySWAAoAOZFBHCAID1hf9OyUJJiKQFADoS0nLLneRaDABYT/wfl5SQqADAQFSOeZyciwEAawj/3ZIkEhMAxBGVc+4mB2MAwDzxf0TiIRkBgAmo3PMIuRgDAPEV/p9I5pGAAMACqFz0E3IzBgCMF//fSy6TdADAQqic9HtyNAYAjBH+XpKxkmaSDQBYkGYtR/UiZ2MAQD/x7yPZR4IBABugclUfcjcGAHou/gmSSpIKANgIlbMSyOEYAIhN+G+XLCaRAICNUTnsdnI6BgAiF//fSDJIHgDgAFQu+w25HQMA3Yv/YxI/SQMAHITKaY+R4zEA0Pkp/0nU8QcAB/cTmMQtAQwAXC/+P5fsJEEAgAtQue7n5H4MAOLfVtgnj6QAAC4ij8JBGAC3i/9QSQPJAABciMp9Q9ECDIDbhL+3ZA4JAAAgnAt7ow0YADeI/y8kKSx6AIDvUDnxF2gEBsDJ4v9bSS6LHYxi9uUV4tvDs0Xi9qli08ZJYtuacWL3svfFgYVjxdGZo8WJz0eJ0xNHiLPjnxMnJ78oUr98VRye+4bY9807YseKj0Ry4sciKXmyWLN7mlh08mvxVc4a5hXihcqNv0UrMABOFP8HJFUscugpSpQXnVwYFvndS98Pi/qVt58Rpc/9U/if/B9dqXyqn8h/+f/EufHDxOHZY8TWdRPEygMzxNyLy3gWYAQqRz6AZmAAnCT+g6fTxQ9iFfyra8S3R2aLPUveFRnvDxK+Zx7UXehjoejFf4u0qS+JzUkTxfxzS3hWoBcqVw5GOzAAThD/cSxoiJbFx+eHt+TVL++KwQ9ZQvC7Q+0UpE57RWzYPJkdAtCDcWgIBsDOJ/2XsYghUr45sUAcmvemKB7xL1sIfldUJ/QVl98ZGH5loM4l8HwhRpZxQwADYDfxv1Oyn8UL3aF+Katf+tlv9Le96HeG7+m/hg8ertvxOYcKIRZULr0TbcEA2EH875lOJz/oghk5q8Mn7c+NGyaqBvRzrPB3RNmwf4iUGa+LpcfmEQsQDSqn3oPGYACsLP5/kJSzWKEjZmatEjtWfiSKXvi3q0S/My58OESsODiT2IBIUbn1D2gNBsCK4n//dNr4Qid38/cseU+UPvcPhL8DLr37rFi9dzqxApGgcuz9aA4GwEri/zdJgMUJ7ZmT8a04sOA/tjnFbzaZ/3kqfE6A2IFuULn2b2gPBsAK4v+opJFFCe23+pXwewc+iLDHQPaY/mLFAV4NQJeonPsoGoQBMFP8n5a0sBjhGut2fiEKRz6CkPeUhP8RJ6eMFPMuUE8AOkXl3qfRIgyAGeI/XBJkEYJiwZnF4uyE5xFunfEM+nv44KSqhkicQQeoHDwcTcIAxFP8R0tCLD5QV/p2L32P7X6DyRmdIFYc4rUAdIjKxaPRJgxAPMT/AxYcKJYdnSPyXn0cgY5jdcHjX7wsZmauJP6gIz5AozAARor/FBYZKNS2tOqUhzDHH2W6VK8E4hA6YApahQFA/MGwO/1nPhmOEJuMeuWSvP5jYhIwARgAtv0hPlv+hSMfRYAthGpHzCsB4HUABsDIA38sKLb82fLnlQDYCw4GYgB6fNWP0/4uP+WfPvlFhNYGrwQSt08lZuHG2wFcEcQAxFzkh3v+LmbWlRXi4geDEVibUNX/AbFl3QRiF26sE0CxIAxA1OV9qfDnYuZeXCay3+iPsNqQvYvfIYbhxoqBlA3GAETc2Ifa/i7m61OLRMFLHPazMylfvSamXyWW4breATQQwgB029KXrn4uZmnKXFH63D8RUQdweuJwMSN7NXEN7bsI0koYA9Ch+P9B6zXNQnEpKw/MCNeeRzydQ8Z7z4a7MxLfoKFy/B/QPAxAe/G/R1LO4nDxL/9j8xB/h3Ju3DDxVQ7NhOA7VK6/B+3DACjxv1OSwaJw9zv/0ufZ9ncyJz4fRaxDe1TOvxMD4G7x7y3Zz2JwL6rfPAf+3MGheW8S89Aelft7YwDcawCWsQjcfc8/ewxX/dzEzuUfEvvQnmUYAHeK/ziC38UV/rJXi4z3ByGKLmwpvGnjRNYAtGccBsBd4j+YoHc36Z++gCC6tWLggH5i1b7prANoz2AMgDvE/wFJMwHvXratHocQuhx16HPehaWsB7iG0oQHMADOFv/fSqoIdndf96t8+q+IIIRfAVEtENqhtOG3GABniv8vJLkEubsP/eWPegzxg+/Y9/XbrA1oj9KIX2AAnHfdL4Xgdvl7fxe29a0Z8i9R+8ZQEfj4LVE/e7JoXPW1aNq5UTTv2yYa1y8TDQu/FIEp74m6d14UNSOeEP7+D7jrUGD/vpwHgBtJccv1QLcYgDkEtbtRbWIdL/YjHhcN878QLenHRLCsWIimRhH1CAZFqNIjWq9cFI3rloq6sSM4DwBuZA4GwBniP5RgdjdLjs8XvmcedKR41Y4eIhpXLBCtmRlChELCiBH0Voim7UkiMOFN4R/wV84DgFsYigGwt/j/XtJAILsYmdSz3hrgONFv2rxaBEuLRLxHKFAnmo/sFYHP3hf+hL6OmtctiR+zXqA9Sjt+jwGwp/j/XJJHELubrWvHO2eL/4UnRPPeLeFteiuM1uzLIjButGPmt3zow2JOxresG2iP0pCfYwDsJf69JDsJXncz9+IyUT7kIfsL/+B/isak5bG904/DaDmVKmrfGOYIE5D65SusHbgRpSW9MAD2MQCTCFpIm/qSvQXpqQdFwzczRajGLyw/QkHRvH+7qH0xwfa3ApYdncP6gRuZhAGwh/g/JgkRsO5mxYGZwp9gXyEKTBwrguUlwnajuSl8vdDO1wmz3hzAgUC4EaUpj2EArC3+v5H4CVZ381XOGpH72hO2FaCGb+eFf1HbebScPRmuQWDXZ6DOjrCW4AaUtvwGA2BN8b9dkkGQws7lH9hTeJ7+m2g+uEs4ZQRLCkXt6/bsuKjOjsy5tJz1BDeiNOZ2DID1DMBighNmZq0SZcP+YcsiPuG7/A4b6tqgep1hRxOwf+FY1hR0xGIMgLXEP4GgBMWOlR/ZTmjq3n5BhHwe4dgRCoqGpXNs91wqBj8kZl9ewbqCjkjAAFhD/PtIKglImJGzWhSPsNd75/qpH1r2ep/u5wP3bxf+BHsdDtyz5F3WFnSE0pw+GADz7/vvIxghXO8/8WN7/fJ//2Wpis3CTaNp0ypbPSP1Okm9VmJ9QQfss3t9ALsbgLEEIYRP/l9dIwps1Oq3dmR/EaquFG4c9bM+tZUJUIdKWWPQCWMxAObV+W8mAEGxcbN9RKVm4EOiNTdLuHY0N4d3P+zyvEqG/6+Ykc0uAHRIs537BdhV/H8iuUzwwTWuvv6kPQQloa9oTj0o3D7U7ofaBbGLCdi2ehzrDDpDadFPMADxMwDzCDq4ruqfTYSkMXGZYGjNhHKzwrshdnhuymCy1qAL5mEA4iP+jxBs0J7jX9hjO7l+2nhU/8a3AakHbWPelqTOY71BVzyCATBW/O+WeAg0aF/4xzPo77bo6GeLpj5mHAr8/ENbGIAjs8ew5qArlDbdjQEwzgAkEWRw3eG/TfY4/Bdu58vopGRwgfAP6Gf9w4DP/2/4tgnrDrogCQNgjPg/TnDBjVz4cIj1f/2/8IRriv3EOhoWTLOFkVuzexrrDrrjcQyAvuJ/p6SEwIL2zDu/VFTZoO1s896tKHwEtwJqnn3Y8s8yffKLrD3oDqVVd2IA9DMACwkquJHdS9+3fsGfMUOECAZR+AhG49rFln+e3oEPilmZK1l/0B0LMQD6iH9fSYiAgh/c/R9t/bv/LSdTUPZIdwEa6kXN89av5rhp4yTWH3SH0qy+GICeif8tkkyCCW5k7sVlwp9g8Vr/H72Gqkc5mnZutLwBSJv6EmsQIkFp1y0YgNgNwGSCCOxa+rc5ZT+KHnVhgCbLFwcqevHfrEGIlMkYgNjE/3eSFgIIOiJ12ivWNgBPPShC9QEEPYYR+OwDy5u7hacXsQ4hEpSG/Q4DEJ343yQ5SfBAZ+RbvPNf4JP/oOSxbgLs3255A7B13QTWIUSK0rKbMACRG4C3CBrojPnnllheIJp2bkLJYz0MWOMXfotf70z/9AXWIkTDWxiAyMT/l5IAAQOdsTlpouU7/oUqPSh5D4Y6QGn1qoCsRYgCpWm/xAB0bwBWESzQFeoUtqVP/78zEgXv6W2A5LWW3+X5Jm0h6xGiYRUGoGvx/zNBAt1RYPH3/43rv0XBeziCZcXWPwewdjzrEaLlzxiAjsX/R5I0AgS6Ykb2asuX/w0W5KLgOozaN4ZauzvgrNGsSYgWpXE/wgD80AAMJTigOxYfn2/tX4bSnIgQpX/1GPVfjrf0sz7/36GsSYiFoRiA68X/dkkpgQHdkbRlirU7/414AuXWaTQsmW3pZ13w0qOsSYgFpXW3YwC+NwCTCAqIhL2L37H2AcCxI1BuvQ4Cblpt6WddNeABMSNnNesSYmESBqBN/O+RNBIQEAknp4y0dgGgye+i3HoVBDq82/o3AU4sYF1CLCjNuwcDkLt2PcEAkZL11lOWFoSGeZ+j3DqNlvOnLG8A1m/9jHUJsbLe1QZgelurXwIBIqZ8yMPWvgK4djHKrddVwOICyxuAPUveZV1CT+jrSgMgv3gvyRkCACJlTsa31i8BvHszyq3TCNXXWf55n/h8FGsTeoLSwF5uNACDePgQDYtOLrS8ILScTEG5dRxWbw18dsJzrE3oKYNcZQDkF75Zks2Dh2hYdnSO5Q1A68UzqLaeBmD4/1n6eWe8P4i1CT1FaeHNbjIAw3noEC2r9k23vAFoProP1dbtHUBQ+Af0s/TzznprAGsT9GC4KwyA/KK9Jfk8cIiWxO1TrX8GYOs6hFsv/a+utPzzznvl/1iboAdKE3u7wQCM4mFDLGzaMNHygtDw7TyUW6fRejXT8s+7eMS/WJugF6McbQDkF7xFUsyDhljYtnqc5QWhfsZElFuvOgDpxyz/vCsGP8TaBL1Q2niLkw3AGB4yxMrupe9ZXhAC48eg3HpVAtyzxfLPW5UDZm2CjoxxpAGQX+w2STkPGGLlwIL/WF4QakcPQbl1Go3rllr+eStmZq1ifYJeKI28zYkG4G0eLvTIACwca3kxqBnyvyi3TqNhwRe2MAAzsjEAoCtvO8oAyC90h8THg4UenQFYM84WghBqbEC9dRiqsZLVn3X5EM4AgO4orbzDSQbgIx4quOEaYLga4OnjqHdPRzAoaob+2/rXAF99nLUJRvCRIwyA9u6/kgcKbqgEGL4KOP8LBLynNwAunLbFs6YSIBhEZTzOAsTDALzKwwQ9mHfBHofCakY8IUQohIr35P3/4pm2eNYnp4xkbYJRvGprAyC/wE2SqzxI0IWra0WVxUvDftcTICsDFe/BqB31lC2e86F5b7IuwSiUdt5kZwPwJA8R9ERVXrODMDSuXIiKx/r6Pz/HFs9YsWPlR6xLMJIn7WwAUnmAoCdZbw6whTBQD8D59/8VSVumsC7BSFJtaQDkB7+fhwd6c3b8c7YRh2BpEWoew6gbO8I2z3j54VmsSzCa++1oADbw4EBvjsweYxtxaNywAjWPdvu/otQ2z9ef8D9iTsa3rEswmg22MgDyA/9aEuTBgd6s2/G5bQSi5vnHRKihHlWPYtTPmWKb55v72hOsSYgHSkt/bScDMJeHBkYw68qKcAMW2+wCrFmMqkf6678gV/j72+fZpsx4jTUJ8WKuLQyA/KB3Sep5YGDYQcC3nrLPLsDAh0SouhJ1j6T076fv2Gf7X7J+22esR4gXSlPvsoMBoOwvGMrBeW/aSihUUxtG16P14hlbPdPq/n3F7EvLWY9g6/LAeov/j6fT8hcMZvWeL20lFv4B/USwpACV7+rk/zsjbfVMs9/oz1qEeKO09cdWNgAU/gHDUf3XK5/qZyvBqJ/6ISrfyWhO2W8vQyc5PHsMaxFsXxhIbwOwiwcE8eDyOwNtJxqtl86j9j9Q/2ZR+8oztnuWa3d+wToEM9hlSQMgP9g9khAPCOLB/q/H2k40akf250Dgjdf+Zn1qu+eo+lHMylzJOgQzUBp7jxUNwCc8HIgXKw/MsJ1wKOref1mIlmaUX46mTatt+QyvjH2aNQhm8omlDMD0tq5/xTwYiBczclYLz6C/21JA1K9et4+W9GPCn/CALZ/f/oVjWYNgJkprb7KSAXiEhwLx5sTno2wpIAr169e1BX8Kc0XNoH/a9tktOvk16w/M5hErGYBkHgjwGiCaOvIPhH8Fu22Eavyi9uWnbfvcMv/zFGsPrECyJQyA/CB9JK08EDCDwpGP2lZM1K9g9WvYPdV+WkTgv6/b17RJtq0ex7oDK6A0t48VDACV/4DbAD1oGOSG64GhuhoRmPCGrZ+Vqj0xh+p/4KDKgD0V/16SfB4EmMXC04vCbVntLCz+px4UzXu3Ovedf1G+qH11oL2fkeTMx8NZc2AllPb2MtMAPMxDAIoC6dQzYPFMqZZBZ532P5Uqagb/0xHPJ3H7VNYbWI2HzTQAK3kAYDZbEj92hMAoAp+8JUJ1tc645795tW2v+t1I2bB/iK9y1rDewGqsNMUAyL/4VkktDwDMZtaVFcL3zIOOMQG1rz1r7+ZBzc22rPDXFSkzXmetgRVRGnyrGQbgCSYfrEL65BcdJTg1g/4hGtcvE6HGBtsV+KkdM8RRz0Kx7Ogc1hlYlSfMMABrmXiwCmvs1iI4UiMw4gnRvGeL5c8GtGZdEnUfvebIZ5D36uOsMbAya+NqAORfeJskwMSDZbi6VhS98G9HClD4tcDrg0VL2hHrnfAvKxb108YJf0Jfx8797mXvs77Ayigtvi2eBmAAkw5WY5dM1E4Voe+aCX34qmi9ctH8e/3+KtHw9XThH/BXR8+359m/idmXV7C+wOoMiKcBSGLCwWqoRG3XBkFR7wiMGSIaV34d3noXoVB8RN/nEU07N4ZvKqjaBW6Y5yOzxrC2wA4kxcUAyL/op5IGJhysyOE5b7hCmG48J9Cw4AvRcvqE7q2Gg/lXw4cR695+wdHb/B1RNaCfWHB2MesK7IDS5J/GwwAMZLLBqiw4uyScuN1mAtrfHqj//CPRuOpr+Wt9U/jcQGvOFRGq9Mqf8MFOyvTWhnsStJxNE837t0vB/1Y0zP/C1k179ODk5BdZU2AnBsbDAGxmosHKnJwy0tXC1SnSGNW++KSoe2dk+MR+7SvPiJqBDzEvHXZr/B+xJHUe6wnsxGZDDYD8C34maWKiwcqoxG37/gBgKhc/GMxaAruhtPlnRhqAIUwy2AGVwBEyiJXVe75kHYEdGWKkAVjPBAOFgcDJ5IxOYA2BXVlviAGQf/DNEj8TDHbh6ugnETSImk0bJ7F+wK4ojb7ZCAPwFyYXomFu/gZxxp8l9nhPitUle8SsvPVx/fs3bZiIoEFUqGqSdP0Dm/MXIwzAJCYWouFo5fnrr5vJfyqba0RmXaFIqTovNpcfEd8UbjXs71eJvHjEvxA2iJidyz9k7YLdmWSEATjFxEKkzJS/9gOtjREVm2kMNoviBo84U/P9bsHsvCRdPseOlR8hbBARZcP+IWZlrmT9gt05pasBkH/g3ZIQEwuRss+b3rOys/KfquZakRVQuwUXYt4tmJG92tFNgoBf/wA3oLT6bj0NANf/IPKt99x1wt9SZ0g9+ia1W9DoEWdrssXe8G7BXjE7v+vdguTEjxE46BL1qmhG9irWL7jqOmCkBmAVEwqRsr0iNe7d6dp2C4rEsaoLIrn8qFjcbrfgq6trRP7L/4fQQadsXTuetQtOYpUuBkD+Qb0kPiYUIsXTVGWJXvVNwRZR0ugN7xac2rMEoYMOKXzpUU7+g9NQmt1LDwPwJyYTImVj2SFh1RHuaIfgwQ00Hdkbvp2SHSgSx6szwjtYK4p3hQ+ysqbBxvxJDwMwnomESClqqLCsAVDd7hA8aE/dW88JEQp1ehC1uqVW5ASKRVr1JbHTc1ysKtnd7ZkTAIswXg8DcJyJhIjK75bsFVYfgf++jvDBd7ScTIkpjmpaAiKvvlSk+6+I3Z60cOyrwlfkAbAQx3tkAOQfcIckyERCJKhfSlYfrZfPI3zQ9uv/vZf0f83UWi/y68vEaX+m2OtNF+tK94v5BZvID2AGSrvv6IkBeIhJhEhYVrRD2GUEPn0HAQTRcj49bjFX39oYfj2mDqTu95267pYKgIE81BMDMIEJhEjIqM21jQFozc0W/oS+iKCLCYwfY2oMKjNA3oA4MKEnBmAfEwjdsahwiwiGgsJOo/7L8Qihi2nNzDA9BteV7iN/gNHsi8kATG9r/xtgAqE71PtOu41gWbHwP/UgYuhC6qd+aIkYVGcFyB9gMErDb47FAPyRyYPumJe/UTSHWoQdR8O38xBEt/H0gyJYXmKZGFxVsoc8Akbzx1gMwGgmDrojteqisOsI1QdEzXBKBLuJxpULLRWD6uYMeQQMZnQsBiCRiYOuUC17G4JNws6jed82hNEl1Ix4QoQa6i0Xg8uLd5JPwEgSYzEApUwcdMUB32lh+xEKibqxIxBIF9B8wJpXVTPrCsknYCSlURkA+X+4l0mDrpiRlxiuhuaE0XqJ4kCOL/rz7shOS/6a7kHlP0uLtpNXwEjujcYADGbCoCt2ek4IJ4366RMQSqeS0NcS1/66Gpdq88grYCSDozEAC5gw6Apfs99RBiDorRA1Ax9CLJ147W/GROu/iZL/UB0QDGRBNAbgPBMGnbG5/IjuCfBKXRvVzeYl4cZ1SxBMpx38k6YuVOmxhQk9X5NDfgGjOB+RAZD/Ym9JCxMGnVHS6NX3Pn5QiM9yhPgkq42p8j8vKRRie4UQ6dVCFDYI0RSPQoNNjaJ2ZH+E00nX/tYvs89ZlFAwXFWTHAMGoDS9dyQG4D4mCzpDdTfTexyt/F78u2JWnhDrSoQ46FPvTIXwNautU52vBR7di3A6hNqXBsgHaq9rqmf8WeQZMIr7IjEAg5go6Izc+lKdf/UIMf1qZAagI6bkCLG4UIit5UKkVavyqm07Cj0ZdR++ioA6gKRNB8WKYiF2e4Q4VyNEufQCwZDVdwFaxYKCzeQaMIJBkRiAqUwUdMTy4l26J7zT/tjFvytm5AqxukSI/V4hLtYK4WmKfLcg3C1wQD9E1MZceOeNDuNiUrYQC/KF2FQmRGqVEFcDQgRarWUC0qsvk2/ACKZGYgB2MFHQEZfr8nVPdnPzjTEAHTFZJv+vC4TYXN6W/HPrO0/+jcvnI6Q2xff0Q2Lm8eKoYmO6NIwri4XYKw3jhRohKkzcLVC9NVSPDXIO6MyOSAxACRMFN/JN4dbwVSV9K6DFT/wjSf57vOoktrZV3Ngoal9+GkG1Idu+Wa1LXHzazjAer4rvTsHxqgzyDuhNSZcGQP4LdzFJ0BFna7J1T3JLi6xhADrbKt60Nx1BtRnZr40QE68EDYmJPXG8TdgYbBZz8zeQe0Bv7urKAPRjguBGFhRsCh9O0nMUNVhX/NuT+ulkhNUmVPXvJ+YfzDQsFtT11LhcR9VGStV58g/oTb+uDMDrTBDcyIlq/cuoJpbawwBMPVcjSoc+hsDagP0z5hkeDyeq4mcAVKdN1XGTHAQ68npXBmAREwTtmZOfFN6O1HNUyj9uYpY9DIBizXpqA1idghefEZMzGg2Phdl5+tee6GocqjxLHgI9WdSVAUhjgqA9h2UC0nuoCn+fZNmLsx+8g9BamCU70uMWC5fr4mcAAq0NYmbeenIR6EVahwZA/g+9JPVMEFxjZl6iqJMJSN+E1nYdz24GYPrJcuEZ+A/E1oIcmzwlrrGwrCi+1wL3+06Rj0AvlMb36sgA/IrJgfbs8Z7UPZmpMr52E/9rbF6WhOBaDHU+Q53TiHcslDTGzwDUtATCZpycBDrxq44MQF8mBq7xVe46UdVcq2siawkJMe2qfQ3AxMyguPLGSwivhVidtN+UWNhYFt9dAGXGyUugE307MgDDmBi4xtaKFN2T2Mlq+4r/NeYcyRVVA/6K+FqAMx++a2qdiJqW+BmA6pa6sCknN4EODOvIAIxnYuAa5U2VuiYwdXJanaC2uwEIF4SZswgBNhl1HmP6yQpT42CfN767ADs8x8lNoAfjOzIAy5gYUCSVHdQ9eWXUOkP8wyViLzWLvJcGIcQmsunbDabHwRdXVd3++BmAyuYadgFAD5Z1ZAAOMTGgKGgo1z15qZa9TjEAim92nxP+hL6IsQlcfvNl8UlmyBJxoF5rxXOoV3PkKOghhzoyAIVMDKwq2a170iqod5b4X+Po1C8Q5DhTOeBBMedonmViYG5+fA2Ap6maPAU9pfA6AyD/i96SIBMDmXWFuietNSXONACfna8TJc89gTDHkV3zllguDrLq4msCNpcfIVdBT1Ba37u9AbiXSYGlRdt1b/nraXKm+F9j5abDCHOcyH15iPj0crPlYmB5nAsDlTX6yFfQU+5tbwAeZELgfM1V3ZPVlnJnGwDF6Q8pE2w01QkPiK/3XrBsDJQ3xdcEqIO65CzoAQ+2NwDDmRB3s7AgWbSG9O11WtsqxKfZzjcAnxzKFmXPUCbYSA5/Md3aVSLL42sAiho85C3oCcPbG4CJTIi7Sa++rHuSUvekHS/+kpeOVYiZs1ci1AZR/HyC+OxCwNpXQ6XRrWuNrwlYV7qf3AWxMrG9AVjBhLiXufkbRFNQ37JmTUEhPs9xvvirtsZPHfSIhAMVIu01ygQbwfLkY7aIBdXnIp4jv76M/AWxsqK9AdjLhLiXo5XndU9Ox6vc8et/anarePKAJ8wrG84K34AHEW0dSft4vG1iQfW5aAnF1wSsLtlDDoNY2NveAJxhQtzJrLz1ItCqb2uzoEyCM3LdYQC+zGr6zgAolnw+D+HWifLB/xZfnK6014FQf3wNwNVACXkMYuFMewNAESCXss+brntSulDjDvFXfH4pcJ0BGLCvTJwbNQwB14G1iXtsFw/z41wYSI0VxbvIZRBTMaBrBqCeCXFny19/i/5VTBYWuMcAjDtXfZ0BuPYqwPPU3xHxnmz9f/KxbWMiJxBfA6CKd5HPIErqwwZA/odbmQx3sr0i1YAtSfeIv+KNNM8PDIBi9qzlCHmMFA3vH66yaNeYWFUc/12AZUU7yGkQLbcqA9CHiXAnqq643mNlsbsMwLAjFR0aAMXhN8cg6DEU/Fm887Tt48IT58JAl+ryyWkQLX2UAbiPiXAfG8sO6Z6EypvcJf6fZoc6FX/Fc9uyRNGgfyPsUbB/xlxHxMbWOBcGUiW8FxduI7dBNNynDEA/JsJ9FDVU6J6ENpa5ywBMy2rp0gAoPlm8DWGPkOxXn7dkrf9YmJwtRCDOhYEu1F4lt0E09FMGIIGJcBdrSvbqnnz8LUJMynaXAfgys7FbA6BI/u/HCHx3bX6f+puYcyTXUfFxOM6FgYKhoFhUuIUcB5GSoAzASCbCXeQE9D+ltNvjLvEP/8q7WBuRARi4u0hkPT8Aoe+C5KWJjouP6VeFaI1zYaAzNVnkOIiUkcoAvM9EuAd1Wljv0RgU4rMc9xmA9077IjIAirFrUkRV/36IfQecf/ct8UlmyJExcq4mvgagNdQqFhZsJtdBJLyvDMA0JsI9ZNTm6Z50UirdJ/6KUameiA2AYtlnsxH8Gyh59l/iy3SPY2NE1cSI90j3XyHXQSRMUwZgKRPhDtT7waDOLX/VFudXue40AE8fis4A9N9fLk6/MgLhb8eni5LDtymcHCd59fE1AM2hFjG/YBM5D7pjqTIAiUyEOzjtz9Q92Zz1u1P8p2YFoxL/a4zadF5UPP0Q4i/Z9uG48JxMzWxydKysKYn/LsCJ6gxyHnRHojIAyUyE85mXv1G0hPS/l6Rqn7vRAHyZ1RyTAVB8NWe168U/57kE8eyugvB8TLlc5/h48TXH1wA0BZvDbb7JfdAFycoA7GQinE9q1UXdk0xWwJ3iH64BcLkhZgOgODD2P64V/6qEB8Q7qw5/XyvhQrXj42VHRfx3AVKqLpD7oCt2KgNwgIlwNrPzkkRDUP/apN8WudcAfHze3yMDMHTHVVEw+FFXGoAVk2dcNxcfnvE5Pl6m5Ai5BuNrANSan52fRA6EzjigDMAxJsLZHPSd1j25lDS6V/wVb5709sgAKMYv3eU68T876jnRf1/ZdfPwZprXFTGjbsvEexyuPEsOhM44pgxAOhPhXGbkJYqaFv37k64vdbcBeO5oRY8NgGLD+E9dI/6qRbJqlXzjHLxw1OOKmJmRq6r1xdcABFobxKy89eRC6Ih0ZQAuMBHOZafnhO5JpapZiIkuFv9Ps0IiQQfxVzyzp1hcHvGMKwzArFkrOr4eedAjJrqkjPSFmvjvAhzwnSYXQkdcUAYgi4lwLr5mv+4JRR1ocvOv/2nZLbqI/3db4OuOi0qHVwk8/NYbXc7BZ1ktroidb0woDFTbWi9m5iWSD+FGspQByGcinMnm8iO6J5P6ViGmZLvbAETaBCgavvlivmPFX7VEVq2RuzQAV+pdEz+FDfE3AXu9J8mJcCP5ygCUMRHOpLTRq/+hIp+7xV/xWUZAdwOQsL9CpL020pEG4OPF27v9/pMu+l0TP4ml8TcA/pY6MSNvHXkR2lOmDEAVE+E81pXu1z2JtITkr9+rGIAPzlTqbgAUIzdfFGXP/MNR4r/5v59E9N3Hna10Tfyo8zNVzfE3Aeo8ELkR2lGlDECAiXAeefX6/8xIr0b8Fa8c9xhiABSTv97sGPG/+OKg8CHHSL732+leV8XQLk/8DUBlc434KpddAPiOgDIArUyEs1hevEv35KFuL83JQ/wVzxwyzgCErwZOmGx78a94+u/i1Q1nIu+RcMzjrtdIOW1ttOM9tlUcI0fCNVoxAA7kcl2+7onjch3CH24ClB00VPwVT+0tFedGDbO1Afhi/vqovvPAQx7XxVJqVfwNgKepmhwJ1xkAXgE4iMWFW+Wvdf2rjSwuRPx72gQoGl7afMG25wG2fDQhttcfWa2uiqWZeUKE4u8BRHL5UXIlfPcKgEOADuJsTbbuCUNdW0L8tRoAVxriYgAUny5Ktp34Z0Tx3v9GpmY2uC6eMmrjbwDKGivJlfDdIUCuATqEBQWbRKsBLX/XliD8353gvlATNwOgSJowxbHv/X+wA3Cp1nXxtKRQmDI2lB0iZ0IZhYAcxInqS7onCtXHHOH/nv+k++JqANR5ANVAxx7v/RN79F0nnK9yZUwVm1AYqLjRQ86EfEoBO4Q5+UmiMaj/5eKt5Yh+e4aneOJqAOxyHmDrR+N7/D3fO+V1ZUwllZqzC5BYup/cSSlgmgE5AdX2U+9R1yrE5GxE/xqTdGwCFC2TFm2xrPhfeuHZmN/7t+f14x53xlW2qtQXfwNQ0FBO7qQZEO2A7Y5q9FHXqv8+4gHK/l7HF1ktpoj/NdZ/PMWi7/1P6/L9hh3xuDa29njN2QVYU7KXHOrydsDHmAh7s8d7UvfE0ByUgkfZX8ObANn9PMC0eYn67nRkhVwZW5/ntK25eI/c+hJyqHs5pgzAASbCvqjSnlXN+t8lOkHZ3x8WAboUMNUAXOsXUDrwn9Z47//hON2/39TMJtfGV1q1ObsAK4t3k0vdyQFlAHYyEfZla0WKIWV/Z1L29wf892y16QYg/Cv5my0Wee9fpPt3m3K5zrXxNdukwkBZgSJyqTvZqQxAMhNhX8qbKnVPCBdrEfuOePW41xIGoO08wGemib/nqb+L15JOG/K9Jl6odnWMXakzZxfg26Id5FP3kawMQCITYU+Syg4akgwWFSD2HTHwkMcyBkCdBzgz6nmT3vuvM+x7fXjG5+oYW1ZkjgFQ/UPIqa4jURmApUyEPVHXePQeefUIfYfd2+LQBMgO5wG2GfDevz1vpnldH2uljfE3AKp/yJKibeRVd7FUGYBpTIT9WFWy25BEsKoYsTezCVDUW+bfbI3je/+Bhrz3b8+LKR7Xx9qmMnN2AS7W5pJb3cU0ZQDeZyLsR1ZA/yLiFU0IvRWaAEXLuk8+j8N9/4cMe+/fngEHMQCqMFCtCYWBgqGgWFS4hfzqHt5XBmAkE2EvlhZtN6Tl7+YyhL4zPr1Qa1kD0H9fmTjx+ihDDcDkrzfH7ftMyWpxfbztN6kwkOomSo51DSOVAUhgIuzF+Zqrui/8mpa2Xx6Ifce8nV5pWQMQrqK3PUfkDn3CEPFfPenLuH6Xz67UU3VSLvEWE+4EtoaCYmFBMnnWHSQoA9CPibAPanGqRar3UKVIEfrOGZHisbQBUPxn7THhG/CgruJ/bMxrov/+8vjWOcjwE3OSdJMKA53yZ5Jr3UE/ZQDuYyLsQ3r1Zd0XfKP0E1NzSLhWbAIULdPnrNFN/HOeSxBDd1yN+3cYd66SuJPMzTfHALSEWsX8gk3kW+dznzIAfZgIezA3f4NoCup/OuhYFcm26yZArbYQ/2tsGjdRh2I/fxNvJKaZ8vnfTucq4DWyA+aYgLTqS+Rc59NHGYBbmQh7kFJ53oCTv0LMyCXRWrkJUCxFgk69MqJHBuCL+etN+/wvp3IT4Boris0xAOqHxtz8jeRdZ3Pr/5PPWpmAeibD2szKWy8CrfpXCDlXQ5LttlObBZoARcvwrVdEweBHYxJ/1XbYzM/+7CEMQHvU9VwzxrGqC+Re51KvtP+aAShkQqzNPm+6IYt8QT4JtjvGn/PbzgAo3ll1WFT27xeV+Ke99lL4WqHZn31KdpDY00guN8cANAabxJz8JPKvMylsbwDOMCHWbvnrb9G/S0hOgOQaCa+f8NrSACjmzPw2YvHPG/J/4rltWZb43FMzG4m9azUosoWoazXHBBypPEcOdiZn2huAvUyIddlekWrI4l5eRHKNhGcPe2xrABTbP/hvt+LvG/BXMXZNimU+8+RLtcReOw75zDEA9a2N4deP5GHHsbe9AVjBhFgXT5P+F4JVwxESawRNgLKCthZ/xdN7S8S5UcO6NAAzZq+y1Gf++HwV8df+IOpVVaTHHBNw0HeaPOw8VrQ3ABOZEGuyseywIYt6A2V/bd0EKFpeSL4kip/9V4fin/zfjy33ed877SP+buCM3xwDUNdaL2bmJZKPncXE9gZgOBNiTYoaPLov6GrK/kZuACzcBChaPly+T1QlPHCd+J9+eXj42qDVPuvoE9wEuJEFJhUGUmOvN5187CyGtzcADzIh1mNNyV5DFvMuD8k0UiZfrHOMAVAs/HLRd+JfOPhRMWLrFUt+zueOYAA64qpJhYFqWgJiRt468rJzeLC9AbiXCbEeOQH9q4A0BIWYQtnfiHn3VJWjDIBi97vviar+/cR7Kw9Z9jOq0suqBDMxeD2ri83bBdjlOUFedg73tjcAvSVBJsU6fFu0w6BrPSTRaHjhmMdxBmDg7qK4tveNuStgVjMx2AFekwoDVTXXhq8kk59tj9L63t8ZAIoBWY+M2jwDWn0KMf0qCTTyJkBC9D/oPANgF6ZcriMOO2BbhXm7AOpKMvnZGUWAbjQAh5gYa7CocIsIhvS/83PaT/KMqglQditCbCITL1YThx2dS8lW9/PNMQDeZj852v4c6sgALGNirMFpf6Yhi3cuZX+jYnpmE0JsIh+d5SpgZ6hXeWaN5PKj5Gl7s6wjAzCeiTGfefkbw/249R5X6kia0TcBqkeITeStNNoCd8ZXuW2dPM0Y5U2V5Gp7M74jAzCMiTGf1KqLhizapYUkzWiZcK4GITaRF1O4CtgV52vM2wVQBcrI17ZlWEcGoC8TYy6z85JEQ1D/I75FDSTLWBiT5kOITWTAQY+YSBx2ytcF5hmAkkYvOdu+9O3IAPyKiTEXVXPbiJFYSrKMhUGHEWHTbwJktRCLXZBfb54JWF96gLxtT37VkQHoJalncsxhRl5iuNqW3qOyWfArKgZUP3oE2AK1AK7UE49dsLbEPANQ2FBO7rYfSuN7/cAAaCYgjQkyB1Vly4ih7gyTKN3bBMjuTMrwE49doMy9MvlmDVWunPxtK9Laa/6NBmARE2QOvmb9W30FWtvuDJMo3d0EyM6MO1dJPHbDThMLA+XVl5K/7cWirgzA60xQ/NlcfsSgPt4kx5hfATisCZBdeSedq4Dd8VlOW48Ps8bK4t3kcfvwelcGoB8TFH9KG726L8rmkBDTKPsbM++frkaALcArqVwFjIRjJhYGyg4UkcftQ7+uDMBdTFB8WVe635BFebKapNgTRh7zIsAWQN3EIB67Z4aJhYHU+LZ4J/ncHtzVqQHQTEAJkxQ/1Ds0vYfKA7PzSIoxH6zKpgmQlZicFSQuI+BirXkG4EpdAfnc+pTcqPcdGYAdTFR8WF68y5DFmFFLMqQJkHOYmtlIXEbA4kLzDEBI/rO0aDt53drsiMQATGWi4oNyzUaMbyj7SxMgJ+0AXK4lLiOksME8E5BRm0tetzZTIzEAg5go41lcuDXsmvUeqjIYibCHTYAu0wTISnx8voq4jJD1peYZANXC/BuZ18jvlmVQJAbgPibKeM7WZBtUmIMk2GPO1yK8FuL907QFjqYwULWJhYHO1eSQ363LfZEYgN6SFibLOBYUbBatBrT89TSRAPXgzZOVCK+FGHOCmwDRsNtjngFoDQXF1wXJ5HnroTS9d7cGQDMB55kw4zhRfcmQxZdcTvLTgyFHEF0r8fwRDEA0TM0RosnEwkCn/ZnkeetxviOt78wALGDCjGFO/gbRGNR/j662RYhPKfurQxOgEKJrMRJUT4CsEPEZBcerzDMALaFWsaBgE/neWiyIxgAMZsKM4XDlWUMW3T4vSU8PpmW1ILpWvAqY1Ux8RsGsPCFMrAskTlZfJt9bi8HRGIB7mTD9mZmXKOpa9b+no7b7Ps8h6enSBCiTJkBWZMqVOuIzSi6ZWBioOdgi5uVvJO9bh3sjNgCaCShl0vRlj/ekIYsttYpkRxMgZzPxYjXxGSVLTSwM1JaXLpL3rUFpZzrflQFIZOL046vcdaKqWX9Lrup/qzrgJDx9+PCMH8G1IB+d4SpgLJQ0mmcA1FkndeaJ/G86ibEYgNFMnH5srUgxZJGdryHJ6cmoVJoAWZG3TtIWOBY2lJm7C3C08hz533xGx2IA/sjE6Ud5kzH9OhcWkOT0ZABNgCzJyBSuAsbCpGwh/C3mGYD61kYxK289GmAuf4zFANwsCTB5PSep7KAhi+tqgASnawngLJoAWZEhhz1ixjmv2FYRFHPochk1e73m7gIc8p1BB8xDafjNURsAzQTsYwJ7TkFDuSELa2UxyU3XJkBZNAGyCoMOecRnp71ib45PFJVVipLySlHZXBOOe/WL9oy/bXt72lXitltjm6NO5ZtnANTNJ3UDCi0whX1daXx3BmACE9gzVpXsNmRRlTWS2HRvA0wTIFN5Ror+pFNesTPbJwo00W9Pcb2n07VwrKrNEE+mGFaHnKw2dxdgnzcdPTCHCT0xAA8xgT0jK2DMXZyNZSQ1mgDZn6cPesSEdK/YmuUTeaU/FP325NZ23+quNSREbn1bYaxFBW3NcYhtEX51YuaoaQmIGewCmMFDPTEAd0iCTGJsLC3abkjLX7UFOolfOrrzn/QqRDkOqIOWH530ik2ZPnG1G9FvT3Z1cQyH0FSfeiG2lrdVx3NzfGfWmWsCdnvS0IX4orT7jpgNgGYCjjORsXGh9qpBCwmxNoJhRxFno+gveS/NKxKv+ER2SeSi354sX3GP105VsxDp1UKsLxXiC5edH/i2yFwDoOqgqHooaEPcON6dvkdiAMYzkdGzsCA53BpT79Eg/8jPKPurO5OzaAJkRBOfsSe8YvVln7hSHJvoX/cKwKPvYVq1N1fcqO6qt4mjG5pplTWaawJ2VKSiD/FjvB4G4E9MZPSkV182ZAGlVCLWhvQAoAmQbryR6hXLL/nExaKei/51hwDLfYa8Uvuufr38o3MCbTtsTq2vsdnkwkC+Zj/6ED/+pIcB6CXxMZmRMzd/g2gK6l99Qx1wmk7ZX2MMwJVGxLsHvHbMK5Zc9IlzhfqK/o2ow2Txu74mxIUaIZLLnVNuW+1y1LaaawK2lKegE8ajNLtXjw2AZgJWMaGRk1J53pCFo+4+I9bG8FlGACGPklEpHvH1Ba84XWCs6LenrMFnmnB5m4RIqxZibYkQU238Gu6Az1wDUNFUhU4Yz6pItD1SAzCECY0MVfZSlb80YszLR6iN4r9naxD1CHjhqEfMO+8VJ/J9cRP99uTVmbyH3a4JV2GDqnLX1nXPTrdyVPGklpC587ep7DB6YSxD9DQAd0tCTGr37PeeMmTBqGSDUBvHK8d9CHwnPH/EI2ad84qUPHNEv6dXAeMxmoJt1+x2euxh1E/5zZ2v0kYvemEcSqvv1s0AaCbgFBPbNTPy1gl/i3GXbWta2ip6qYpnn1IHQDdUsZinaAJ0HUMPe8T0s15xKNcnik0W/euuAlZa0wB0tFbP1bQV7JpuweuGyqSYPVSPFHTDEE5FquvRGIBJTGzXbK9IjdviaZS/OC7WykVUau/3kVZgajZNgML196Xof37GK/Zd/b7+vtW46ikVdhwVTUKkVgmxWvqXKRZZr9kBc+ekqKEC3TCGSUYYgL8wsV3jaTKn4LZ6H6k6A26vcM5p5bjeAMh0bxOggYc8YvIpr9iV4xOFFhX99hRWeIXdh7rNk1/fdhhvcaF55YpXWmAzZW3pPrRDf/5ihAFQ7YH9TG7HbCw7bJkEU9LYllwWcGiQJkCd1N//JN0rtmX5RL4NRP9GjDpka9ZQxb0u17UZ+Hi3O1Y7E2aOvPpS9ENf/F21/43ZAGgmYD0T3DFFDR5LJhdV+lRtPS4rojFKp2cAXNAESNXfH3fSKzZn+kRuqf1Evz0VjVXCyaO6RYjT/rbXe0a3O95Sbv73XVWyBw3Rj/XRaHq0BoDrgB2wpmSvLRKLaoxytkaIdSXWeQ9pBd4+Ve3Y+vsfpHlFUg/q71uRgkC5cNMobWyrALrCgHbH6s8LmFwYKCdQjI7E+fpfrAbgZ5ImJvl6VADbbah7wOrakvoF8OVVdxuA5456HVV//+0TXrFGp/r7ViSnpkS4dbRo5332eoX4WqdyxYd85n+v5cU70ZKeo7T5Z4YZAM0EbGaiv+fboh22Tyohrc7AHm/830HSBEgf3jzuFSsu+cQlh4r+9VcBiwTj+1091e5YGfmZMa5d9QOg1eTCQJl1BehJz9kcrZ7HYgAGMtHfk1Gb57ik4mlq65CmTig73QBMs3EToNdTvWJphk+cL3K+6F9XDMhXgvJ3Miq1dseJpUJ8HsVrvrN+s3+EhMTSou1oSs8YGA8D8FNJA5O9Viwq3CKCoZCjE4pqHKKqhq12aPGhLzPt1QTo5RSPWHTRJ84Uukv025NfUYHSR7izV9wgxJEI2h0vsEBhIPVjCl2JGaXJPzXcAGgmIIkJXytO+zNdlVBUuVO13aiqm33ukEOEdmgC9OJRj5h/3itOFvhcK/o30hxqQeGjHM3BtuI/qt1xR1eEc+vN/Xzqx9Tiwq2IeWwkxaLlsRqAAW6f8PkFm0RLqNW1yUQVH1IJQ9U+n5lLEyC9GS5Ff/Y5rziWh+h3RGVzDYrew6HaHZ+X07hZGvqv5BpebYE3K+drchDz2BgQTwNwmyTg5glPrbpIBmk3yhrbThMvLLCXAXjtRKVlRH/YEY/46qxXHLZY/X0rUlzvYdHpPLxN5n+G1lBQfF2YjKBHh9Li2+JmADQTsNatEz47L0k0BJvIGJ0MVcjkRLUQy4us3SZVFUZ6+pC5oj/4sEd8ccYr9qv6+wh7xOTWlrLQHDrO+LMQ9ehYG6uO98QAPOHWCT/oO80qjXCoMqdqm3G9zNef5dAE6Fr9/SmnvWK3TervW/ImQHUxi8uhQ71aXVCwGWGPnCfMMAC3Smrd1/I3UdS21LNKY9reazuEtLVcyLm0wA2ArPg1AXpGiv7EU16xPdue9fctVwvAhwFw8jhZfRlhjwylwbfG3QBoJmCl2yZcVazyt9SxQnUYRQ1C7PO29SZ3YhMgVX9/fLpXJGf5RF4poq0Xl0oLxM7iNHHKf0X4mv0sJAeO5mCLzAsbEfjuWdkTDe+pAXjYzfX/1buqgMM6k5k1fM1CHKsUYkkc26NOuqB/E6D+UvQ/TPOKDZk+kYPo60aGFP1dRWliUf6WDutx7PGmicy6QtEYbGYxOWQcr8pA4LvnYTMNQC9JvpsfwFe560RS2UFxsTZXNJF8dBnqepLqhramRP/mJ+15V6cmQKr+/rsnvGLdZZ/IKkGs9RP9fLFTiX7elqjWozLn6pZOaaM3XGGOYc+hzNzc/A2IfOco7e1lmgHQTMBHPIg2ZuYlii3lR8O/RFpdXCNA163AUFuvdHVX+QudmxYNT/H2SPT/c9wrVl7yicvFiLXeov91FKLfFWobeWvFMXGh9qqobeXsjt1GSuV5tKVzPuqpfuthAPpIWnkY1zMnP0ns9BwXefWlji8XHK+hZjG/vq2S2aweNi36NDu2JkBjUr1iWYZPXChCrPXiYlj0T+gm+t017zrkOyPjqAyTboOhrlura9doyg9QmtvHdAOgmYBkHkjXVQP3edNFcQPFS/Qc5U1CHPYJsajA2CZArxzziG8u+sTZQsRaT9HfERZ984q+zMpbLzaUHRKn/JkcJrTwOFR5Fh35Icl6aLdeBuARHkjkDYQOy4D2NFWxsnUc/hZ1dUiIlcWRFR/qrgnQSykeseCCV6RTf19H0c8Li/7CvGTLrs093pMiK8BhQiuNQGuDmCnNGvpxHY9YyQDcJCnmoUTHsqId4njVRVHVXMsq1/XwkBAX5ZQmlQoxNSfyJkAjjnrEnPNecTwf0deLC2HRP25Z0Y/sMKGPw4Qmj/2+U2jG9yitvckyBkAzAZ/wYGJnVcme8L3mOg4q6TpU8aGcgBDbK9oanlwzAOO0JkDPHfGIGee84kge9ff1FP3tNhT97g4Tbqs4Fr7tw2HC+I+alkD4kDVaEeYTvXRbTwNwjyTEw+n5L4/E0gPhrliN9BvQfZQ0CnHAJ8SavHqx76oX0Uf0YztMWLyz3WHCIAsrDkPVekAjwhp7j+UMgGYCdvGA9L1WuLn8iLhcl0//c73PDDTVIdw95Hxpblj0F+S5u267Oky4seyQOO3PpE2xgaO6pS78A8nlurBLT83W2wA8iXAb14Fwe0WquBooEUF+cfR4FNRUIOIxiv62wlTXi35khwmLOEyo89jhOe72+HrSygbgx5JykoDx7yNVgilsqOBwUozjSmUhgo7ox+WV3tqSfeHDvhwm7PlQOywu3gVQ2vpjyxoAQWXAuLOwIFkc9J0RZY2VZIcoRoYnH3HvgnOa6M/P28Q6M+gwIQd+YxtbK1Ko/GdhA3CXpJ7FHn+WFG0TKVUXeA8ZwbhUXoDQ/0D0r4qtiH7cu4uqQjccJox8qBoqLowVpal3Wd4AaCZgLovbXFYU7wr31FbXZxg/HDllpYj+d6J/DNG3zGHCwxwmjGCow9Eui4+5Rmi1UQbg15Igi9oarC3dJ87WZIt6WheHR0swKIrL3VnsR33vs4g+hwltPtR5ChfFgtLSX9vGAGgmYAOL2FrMyFsXrn1+qTZPNAXde63Q2+B3oejniC1S9Och+rZdu8rIH6/KEGUcJgwP1YbdJc9/g1E6baQBuJ+Fa+3tRnWYJlv+unDbu8dcfymiD444TJgRPkzY4EoDUNTgccvzvt92BkAzAaksVuszN3+D2OU5ET6I5IZfFpd9BY4V/TMlSvRTpOhvJLZddphQNRkraCh3laFfV7rf6c821UiNNtoAUBjIZiwo2BxuvFHS6HVs0rhYke840U9G9OGGw4Rn/FmOP0yofrRQ+Me6BkB1CbzKorQn3xRuFUcrzwlPU7WzdgDKCx0g+tlh0Z+L6EME63iv92T4dV+TAw8Tri7Z49Rnd1Wvrn+mGADNBLzKInRA85OiHeJEdUa4HrfdR15ZOaIPrj1MuE4dJqzOCBcPc8Irv5xAsVOf16tG63M8DMBtkkoWn3NQjlvdVQ7Y8PBRQ2uTrUT/dFj0jyL6YAjzCzaFe4yow4QBGx8mVHVPHPZslGbeZnsDICgP7Og65+tLD4gLtVdtc0+5NOCzhehvDov+BuIM4nyYcJctDxNm1hVS9tfCBuAOiY8F5vzWxZl1BaIl1GrZRJFdVYzoA0TYgXSTdpiwqrnW0gZAvcpYVrTDKXOvtPIOxxgAzQS8zaJySeLITxI7KlJFbr31WhdnePMtI/qnSrLCoj8H0QfbHCZMt+xhQlXgzCFz/Xa8dDmeBuC26bQKdmXBEpU0ihoqrHEFsNw8A1Ckif6mwiOIPjjiMKE6GFzeVGmZXYDFhdvsPrfl8Xj3H3cDoJmAMSwe9/J1YXK485mZCSOzrAjRBzDsMGGeqYcJ1Xkkm8/lmHhqcrwNwC2SYhYMLC3aLlKrLsa9UElhuScuop+O6IOLUafyj1SeE4VxPkyoXjmqJko2nTeljbc41gBoJmAUCwTas7J4t0j3XxG1LfWGJoeapoCBou+Vop8pNiL6AD88TFgev8OEZ2qy7DpXo+Ktx2YYgN6SfBYGdISq7X2uJkc0BJv0//VfW2GQ6B8OJzmeH0D3LC7cKvZ508MFfIzoStoaag2XNLfZvChN7O14A6CZgOEsBOj6kFFi+ArSpbp80axTksisLNRF9E8i+gC6rXNl+vU+TKh2FG02F8PN0GKzDMDNkmwWAESCam6iWp+qXww9eZ+Y4cnvgehfERsQfQDDDxOqK8SXeniYsDnUEv6zbPK9lRbe7BoDoJmAQQQ8RItqXbzbkxauVBZtHfOM8gJEH8CWhwkroq4ponYVbPI9B5mlw2YagF6SMwQ5xIp6z3fAd1qUNvoiqwJYVtKl6BdK0U8Li/4hRB/AggXGVLVRdcgvksOEqliR+sFg8e+lNLCX6wyAZgL6Etig18GilMrzwtvs7/R6kKq+15noJ0nRV68amEsAu6z5bd0eJkypumD179HXTA021QBoJmA9wQx6srx4p0irviT87VoX+xr8N4j+ZZFUgOgDOOsw4SVR0VT1fffPYFN458Cin3u92fprBQNwj6SRIAYjWFOyN3z/+HJ1gTgRFv2DYlYuog/g7NeDm8QOz/HwYcKdnhNW/IxK8+5xvQHQTMAkghYAAFzCJCtor1UMwO2SUoICAAAcjtK62zEA15uAoQQGAAA4nKFW0V0rGYAfSdIIDgAAcChK436EAejYBPyZAAEAAIfyZytprqUMgGYCVhEkAADgMFZZTW+taAB+KQkQLAAA4BCUpv0SAxCZCXiLgAEAAIfwlhW11qoG4CbJSYIGAABsjtKymzAA0ZmA30laCB4AALApSsN+Z1WdtawB0EzAZAIIAABsymQra6zVDcAtkkyCCAAAbIbSrlswAD1vGRwimAAAwCYozeprdX21vAHQTMBCAgoAAGzCQjtoq10MwJ2SEoIKAAAsjtKqOzEA+pqAxwksAACwOI/bRVdtYwA0E5BEcAEAgEVJspOm2s0A3C3xEGQAAGAxlDbdjQEw1gQ8QqABAIDFeMRuemo7A6CZgHkEGwAAWIR5dtRSuxqAn0guE3QAAGAySot+ggGIrwn4vaSZ4AMAAJNQGvR7u+qobQ2AZgLGEoAAAGASY+2soXY3AL0k+whCAACIM0p7emEAzDUBfSSVBCMAAMQJpTl97K6ftjcAmglIICABACBOJDhBOx1hADQTsJigBAAAg1nsFN10kgG4XZJBcAIAgEEojbkdA2BNE/AbiZ8gBQAAnVHa8hsnaaajDIBmAh6ThAhWAADQCaUpjzlNLx1nADQTMImABQAAnZjkRK10qgFQ9QF2ErQAANBDdtr9vr+rDIBmAn4uySN4AQAgRpSG/NypOulYA9CuX0ADQQwAAFGitOP3TtZIRxsAzQQMJZABACBKhjpdHx1vADQTMIdgBgCACJnjBm10iwHoLUkhqAEAoBuUVvTGADjLBPxCkktwAwBAJyiN+IVbdNE1BkAzAb+VVBHkAABwA0obfusmTXSVAdBMwAOSZoIdAAA0lCY84DY9dJ0B0EzAYAIeAAA0BrtRC11pADQTMI6gBwBwPePcqoOuNQCaCVhG8AMAuJZlbtZAtxsAdT1wP4sAAMB17HfLdT8MQOcm4E5JBosBAMA1qJx/p9v1z/UGQDMB90jKWRQAAI5H5fp70D4MQHsT8AeJn8UBAOBYVI7/A5qHAejIBNwvCbBIAAAch8rt96N1GICuTMDfJI0sFgAAx6By+t/QOAxAJCbgUUkLiwYAwPaoXP4o2oYBiMYEPC0JsngAAGyLyuFPo2kYgFhMwHBJiEUEAGA7VO4ejpZhAHpiAkazkAAAbMdoNAwDoIcJ+IDFBABgGz5AuzAAepqAKSwqAADLMwXNwgBgAgAAEH/AAPA6AACAbX/AAPT8YCC3AwAArHHanwN/GIC4XxGkTgAAgLn3/LnqhwEwrVgQFQMBAMyp8EeRHwyA6WWD6R0AABA/GinviwGwUgMhuggCABiPyrU09sEAWK6VsJ/FCQBgGCrH0tIXA2BJE/AHSTmLFABAd1Ru/QNagwGwsgm4R5LBYgUA0A2VU+9BYzAAdjABd0r2s2gBAHqMyqV3oi0YADuZgN6SZSxeAICYUTm0N5qCAbCrERjHIgYAiJpxaAgGwAkmYLCkmQUNANAtKlcORjswAE4yAQ9IqljcAACdonLkA2gGBsCJJuC3klwWOQDAD1C58bdoBQbAySbgF5IUFjsAwHeonPgLNAID4JYbAnNY9AAA4VzISX8MgOuMwFBJAwkAAFyIyn1D0QIMgJtNwO8leSQDAHARKuf9Hg3AAGACctf+XLKTpAAALkDlup+T+zEA8L0J6CWZJAmRIADAgYS0HNeLnI8BgI6NwGPTaSsMAM5C5bTHyPEYAOjeBPxmOh0FAcAZqFz2G3I7BgAiNwG3SxaTPADAxqgcdjs5HQMAsRmBBEkliQQAbITKWQnkcAwA9NwE9JHsI6kAgA1QuaoPuRsDAPreEhg7na6CAGBNmrUcxSl/DAAYWDjoMskGACzEZQr7YAAgPibgJ5J5JB0AsAAqF/2E3IwBgPgagUckHhIQAJiAyj2PkIsxAGCeCbhbkkQyAoA4onLO3eRgDABYwwg8LikhMQGAgagc8zg5FwMA1jMBd0oW0k8AAAyo469yy53kWgwAWNsI9JVkkrQAQAdULulLbsUAgH1MwC2SyZIWEhgAxECLlkNuIadiAMCeRuB3kpMkMwCIApUzfkcOxQCA/U3ATZK3JAESGwB0QUDLFTeROzEA4Cwj8EvJKpIcAHSAyg2/JFdiAMDZRuDPkjQSHgBoueDP5EYMALjHBPxIMlRSSgIEcCWlWg74ETkRAwDuNAK3SyZJGkmIAK6gUVvzt5MDMQAAygjcI1lPcgRwNGqN30POAwwAdFZE6AyJEsBRnKGYD2AAIBIT0EsySJJN4gSwNdnaWu5FbgMMAERjBG6WDJfkk0gBbEW+tnZvJpcBBgB6YgR6S0ZJikmsAJamWFurvcldgAEAvfsLjJGUk2gBLEW5tjap2w8YADDUCNwmeVviI/ECmIpPW4u3kZsAAwDxNAJ3SD6SVJKIAeJKpbb27iAXAQYAzN4ReFWSQ2IGMJSr2lrjFz9gAMByXQeflKSSqAF0JVVbW3TpAwwAWN4M3C/ZIAmSvAFiIqitofvJKYABADsagV9L5krqSegAEVGvrZlfk0MAAwBOMAJ3aYeWuEII0PlVPrVG7iJnAAYAnGgEfqy9y9wlCZH0weWEtLWg1sSPyRGAAQA3dSD8hAqD4NKKfZ/QmQ8wAMDtgdy1j0iSJa2IAziUVi3GH+E0P2AAAH5oBvpo70FpQAROasyjYroPaxwwAADdGwHVkvhhyUpJLSICNqNWi92HacULGACA2M3ArZInJGslAcQFLEpAi1EVq7eydgEDAKB/2eEBkiRJA6IDJtOgxeIAyvMCBgAgfmbgp5KBks2SJsQI4kSTFnMq9n7KWgQMAIC5ZuBnkiGS9RI/IgU649diS8XYz1hzgAEAsKYZuFnyF8kkySkKDkGMBXpOaTGkYulm1hZgAADsZwju1n65rZL4EDfoBJ8WIypW7mbtAAYAwHnXC/8kGS85TrdC13fbO67Fwp+4rgcYAAB3GYI7JA9JJkj2cc3Q8df09mnPWj3zO1gDgAEAgPbnB/4oGS1JlJQinLalVHuGo7Vnynt8AAwAQFSm4F7JYMkCyXlJC+JqOVq0Z7NAe1b3ErsAGAAAvQ1Bb8l9kkGSqZIdkhJEOG6UaHM+VXsG6ln0JjYBMAAAZhmDuyT9JK9LFknSJPUIdszUa3O4SJtTNbd3EWsAGAAAu9w6+JWkr2SYduJ8meSQpNDltxCC2hwc0uZkvDZHfbU541Q+AAYAwNGvE9QZgwclwyUTJSskeyVnNIGst+mv90LtO+zVvtNE7Ts+qH1ntu0BMAAA0I1RUB0R+2jvu9VWeIJkpOR9yTTJUu3Ee7Jkp+SA5JgkXXJBkqX1pS+TVGlX4lo1Atp/V6b9O1na/ydd+zMOaH9msvZ3LNX+zve1z5Cgfab7tM9IJzwAG/D/AfjBglLT/sHPAAAAAElFTkSuQmCC"></img></a>`
                $('.int_msg_in_con').last().append(text_str_response);
                $('.int_msg_in_con').last().after(str_clr);
                chatbot_container_auto_scroll()
            }

            //ajax call function
            function ajax_call(str_url) {
                $.ajax({
                    url: str_url,
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    data: JSON.stringify(request_body),
                    beforeSend: function () {
                        $('.int_bot_conversation').last().append(str_int_msg_out_icon);
                        $('.int_waiting_icon').last().append(str_int_msg_in_con);
                        $('.int_msg_in_con').last().append(str_waiting_icon);
                        $('.int_msg_in_con').last().after(str_clr);
                        chatbot_container_auto_scroll()
                    },
                    success: function (data) {
                        $('.int_waiting_icon').remove();
                        previous_conversations.push(data)
                        localStorage.setItem("previous_conversations", JSON.stringify(previous_conversations));
                        data.forEach(function (item, index) {
                            if (item.hasOwnProperty('text')) {
                                bot_text_res(item.text)
                            }
                            if (item.hasOwnProperty('buttons')) {
                                bot_button_res(item.buttons)
                            }
                            if (item.hasOwnProperty('image')) {
                                bot_image_res(item.image)
                            }
                            if (item.hasOwnProperty('attachment')) {
                                type = item.attachment.type
                                if (type == 'video') {
                                    video_src = item.attachment.payload.src
                                    bot_video_res(video_src)
                                }
                                if (type == 'pdf') {
                                    pdf_src = item.attachment.payload.src
                                    bot_pdf_res(pdf_src)
                                }
                                if (type == 'location' ) {
                                    location_src = item.attachment.payload.src
                                    bot_location_res(location_src)
                                }
                            }
                            chatbot_container_auto_scroll()
                        });
                    },
                    afterSend: function () {
                    }
                });
            }

            //getting href values of 'a' on click in the chat window 
            $(document).on('click', 'a.res', function (e) {
                e.preventDefault();
                var new_messege = $(this).attr('href');
                var button_text = $(this).text()
                request_body.message = new_messege
                user_res_render(button_text)
                user_res_object = {
                    recipient_id: "user_res",
                    text: button_text
                }
                user_res_array = []
                user_res_array.push(user_res_object)
                previous_conversations.push(user_res_array)
                localStorage.setItem("previous_conversations", JSON.stringify(previous_conversations));
                chatbot_container_auto_scroll()
                ajax_call(str_url)

            });

            //getting href values of 'a' on click in the chat window for pdf
            $(document).on('click', 'a.pdf_res', function (e) {
                var new_messege = $(this).attr('href');
                var button_text = 'opening pdf'
                request_body.message = new_messege
                user_res_render(button_text)
                user_res_object = {
                    recipient_id: "user_res_attachment",
                    text: button_text
                }
                user_res_array = []
                user_res_array.push(user_res_object)
                previous_conversations.push(user_res_array)
                localStorage.setItem("previous_conversations", JSON.stringify(previous_conversations));
                chatbot_container_auto_scroll()
            });

            //getting href values of 'a' on click in the chat window for location
            $(document).on('click', 'a.loc_res', function (e) {
                var new_messege = $(this).attr('href');
                var button_text = 'opening location'
                request_body.message = new_messege
                user_res_render(button_text)
                user_res_object = {
                    recipient_id: "user_res_attachment",
                    text: button_text
                }
                user_res_array = []
                user_res_array.push(user_res_object)
                previous_conversations.push(user_res_array)
                localStorage.setItem("previous_conversations", JSON.stringify(previous_conversations));
                chatbot_container_auto_scroll()
            });

            //generating random username 
            function GetUserName() {
                var text = "";
                var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

                for (var i = 0; i < 5; i++)
                    text += possible.charAt(Math.floor(Math.random() * possible.length));
                return text;
            }

            //getting the input msg and calling api
            $(".int_bot_send").click(function () {
                user_input_text = $(".int_bot_new_message").val();
                $(".int_bot_new_message").val('');
                text_str_response = `<p class="res">${user_input_text}</p></div>`
                $('.int_bot_conversation').last().append(str_int_msg_in);
                $('.int_msg_in').last().append(str_int_msg_out_con);
                $('.int_msg_out_con').last().append(text_str_response);
                $('.int_msg_out_con').last().after(str_clr);
                chatbot_container_auto_scroll()
                user_res_object = {
                    recipient_id: "user_res",
                    text: user_input_text
                }
                user_res_array = []
                user_res_array.push(user_res_object)
                previous_conversations.push(user_res_array)
                localStorage.setItem("previous_conversations", JSON.stringify(previous_conversations));
                request_body.message = user_input_text
                ajax_call(str_url)
            });


            $('.int_bot_new_message').keypress(function (event) {
                $(".int_bot_send").show();
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == '13') {
                    event.preventDefault();
                    user_input_text = $(".int_bot_new_message").val();
                    text_str_response = `<p class="res">${user_input_text}</p></div>`
                    $('.int_bot_conversation').last().append(str_int_msg_in);
                    $('.int_msg_in').last().append(str_int_msg_out_con);
                    $('.int_msg_out_con').last().append(text_str_response);
                    $('.int_msg_out_con').last().after(str_clr);
                    user_res_object = {
                        recipient_id: "user_res",
                        text: user_input_text
                    }
                    user_res_array = []
                    user_res_array.push(user_res_object)
                    previous_conversations.push(user_res_array)
                    localStorage.setItem("previous_conversations", JSON.stringify(previous_conversations));
                    request_body.message = user_input_text
                    ajax_call(str_url)
                    $(this).val('');
                    chatbot_container_auto_scroll()
                    return false;
                }
            });

            // calling api for first time automatically
            $(".int_bot_action").click(function () {
                if (previous_user == 0 && localStorage.getItem("username")) {
                    request_body.message = initPayload
                    ajax_call(str_url)
                    previous_user = 1
                } else {
                    previous_conversations = JSON.parse(localStorage.getItem("previous_conversations"))
                    if (previous_conversations === null) {
                        previous_conversations = []
                        request_body.message = initPayload
                        ajax_call(str_url)
                    }
                    previous_conversations.forEach(function (data, index) {
                        data.forEach(function (item, index) {
                            if (item.recipient_id == "user_res") {
                                user_res_render(item.text)
                            }else if (item.recipient_id == "user_res_attachment") {
                                 user_res_render(item.text)
                            } else {
                                if (item.hasOwnProperty('text')) {
                                    bot_text_res(item.text)
                                }
                                if (item.hasOwnProperty('buttons')) {
                                    bot_button_res(item.buttons)
                                }
                                if (item.hasOwnProperty('image')) {
                                    bot_image_res(item.image)
                                }
                                if (item.hasOwnProperty('attachment')) {
                                    type = item.attachment.type
                                    if (type == 'video') {
                                        video_src = item.attachment.payload.src
                                        bot_video_res(video_src)
                                    }
                                    if (type == 'pdf') {
                                        pdf_src = item.attachment.payload.src
                                        bot_pdf_res(pdf_src)
                                    }
                                    if (type == 'location' ) {
                                        location_src = item.attachment.payload.src
                                        bot_location_res(location_src)
                                    }
                                }
                            }
                        });
                    });
                    chatbot_container_auto_scroll()
                }
            });

            //function for rendering user selected button in the chat window
            function user_res_render(button_text) {
                text_str_response = `<p class="res">${button_text}</p></div>`
                $('.int_bot_conversation').last().append(str_int_msg_in);
                $('.int_msg_in').last().append(str_int_msg_out_con);
                $('.int_msg_out_con').last().append(text_str_response);
                $('.int_msg_out_con').last().after(str_clr);
            }


            if (auto_start) {
                //opening chatbot automatically after 10 seconds
                setTimeout(function () {
                    if ($('#open_close').attr('value') == 'chatting') {
                        $(".int_bot_action").trigger('click');
                    }
                }, 5000);
            }

        });
    }
}


